<?php
class Content extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
	}

	public function index($page = null)
	{
		if ($page === null) {
			$data['title']		=	"Tentang Sipardi";
			$data['data']		=	$this->crud_model->select_by_field_row("tbl_konten", "about", ["id_konten" => "1"]);
			$data['page']		=	"about";
		} else {
			$page = str_replace("-", "_", $page);
			$data['data']		=	$this->crud_model->select_by_field_row("tbl_konten", $page, ["id_konten" => "1"]);
			$data['page']		=	$page;
			$data['title']		=	str_replace("_", " ", $page);
		}
		$this->load->view("frontend/page", $data);
	}
}
