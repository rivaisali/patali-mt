<?php
class Toko extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		if (!$this->session->userdata('user')) {
			$this->load->helper('url');
			$this->session->set_userdata('last_page', current_url());
			redirect('/login');
		}
		if (!penjual("id_penjual")) {
			redirect("frontend");
		}
	}

	public function index()
	{
		redirect("frontend");
	}

	public function saya()
	{
		$data['title']		=	"Toko";
		$data['page']		=	"toko/index";
		$data['subpage']	=	"profil";
		$data['profil']		=	$this->crud_model->select_one("penjual", "id_user", user("id_konsumen"));
		if (penjual("status_penjual") != $data["profil"]->status_penjual) {
			$this->session->set_userdata("penjual", [
				"id_penjual" => $data["profil"]->id_penjual,
				"nama_penjual" => $data["profil"]->nama_penjual,
				"status_penjual" => $data["profil"]->status_penjual
			]);
		}

		$this->load->view("frontend/main", $data);
	}

	public function edit()
	{
		$this->load->model("users_model");
		if ($this->input->post('simpan')) {
			$this->form_validation->set_rules('nama_penjual', 'Nama Penjual', 'trim|required');
			$this->form_validation->set_rules('no_telpon', 'No Handphone', 'trim|required|numeric|max_length[15]|min_length[11]');
			$this->form_validation->set_rules('alamat_lengkap', 'Alamat Lengkap', 'trim|required');
			$this->form_validation->set_rules('kota_id', 'Kab / Kota', 'trim|required|callback_check_default');
			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required|callback_check_default');
			$this->form_validation->set_rules('kelurahan', 'Kelurahan', 'trim|required|callback_check_default');
			$this->form_validation->set_rules('buka', 'Buka', 'trim');
			$this->form_validation->set_rules('tutup', 'Tutup', 'trim');
			$this->form_validation->set_rules('gambar', 'Foto', 'trim|callback_check_file');
			$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');
			if ($this->form_validation->run() == true) {
				$cek_nama_sama = $this->crud_model->cek_data_where_array("penjual", ["penjual_seo" => strtolower(url_title($this->input->post("nama_penjual", true))), "id_penjual <>" => penjual("id_penjual")]);
				if ($cek_nama_sama) {
					$penjual_seo = strtolower(url_title($this->input->post("nama_penjual", true)));
				} else {
					$penjual_seo = strtolower(url_title($this->input->post("nama_penjual", true) . "-" . rand(1, 99)));
				}
				$data = [
					"nama_penjual" => $this->input->post("nama_penjual", true),
					"penjual_seo" => $penjual_seo,
					"no_telpon" => $this->input->post("no_telpon", true),
					"alamat_lengkap" => $this->input->post("alamat_lengkap", true),
					"kota_id" => $this->input->post("kota_id", true),
					"kecamatan" => $this->input->post("kecamatan", true),
					"kelurahan" => $this->input->post("kelurahan", true),
					"buka" => $this->input->post('buka', true) . ":00",
					"tutup" => $this->input->post('tutup', true) . ":00",
				];

				$config['upload_path']     = './uploads/users/';
				$config['allowed_types']   = 'jpg|png';
				$config['max_size']       	= 2000;
				$config['max_filename'] 	= '255';
				$config['encrypt_name'] 	= TRUE;

				$detail		=	$this->crud_model->select_one("penjual", "id_penjual", penjual("id_penjual"));
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('gambar')) {
					$data_file = $this->upload->data();
					$data['foto']	=	$data_file['file_name'];
					if ($detail->foto != "default.jpg") {
						unlink('./uploads/users/' . $detail->foto);
					}
				}

				$simpan		=	$this->crud_model->update("penjual", $data, "id_penjual", penjual("id_penjual"));
				if ($simpan) {
					$session = $this->users_model->get_data_penjual_after_update(penjual("id_penjual"));
					$this->session->unset_userdata('penjual');
					$this->session->set_userdata('penjual', $session);
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Profil Toko Berhasil Diperbarui"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Profil Toko Gagal Diperbarui"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect("toko/saya");
			} else {
				$data['title']		=	"Edit Profil";
				$data['page']		=	"toko/index";
				$data['subpage']	=	"edit";
				$data['data']		=	$this->crud_model->select_one("penjual", "id_penjual", penjual("id_penjual"));
				$data['kota']		=	$this->crud_model->select_all("rb_kota");

				$this->load->view("frontend/main", $data);
			}
		} else {
			$data['title']		=	"Edit Profil Toko";
			$data['page']		=	"toko/index";
			$data['subpage']	=	"edit";
			$data['data']		=	$this->crud_model->select_one("penjual", "id_penjual", penjual("id_penjual"));
			$data['kota']		=	$this->crud_model->select_all("rb_kota");

			$this->load->view("frontend/main", $data);
		}
	}

	// =====================================================================================================================================
	// produk
	// =====================================================================================================================================

	public function produk($id_produk = null)
	{
		if ($id_produk === null) {
			$data['title']		=	"Produk";
			$data['page']		=	"toko/index";
			$data['subpage']	=	"produk";
			$data['data']		=	$this->crud_model->select_all_where("produk", "id_penjual", penjual("id_penjual"));
		} else {
			$detail				=	$this->crud_model->select_one_where_array("produk", ["id_produk" => $id_produk, "id_penjual" => penjual("id_penjual")]);
			if (!$detail) {
				redirect("toko/produk");
			}
			$data['title']		=	"Detail Produk";
			$data['subtitle']	=	$detail->nama_produk;
			$data['page']		=	"toko/index";
			$data['subpage']	=	"produk_detail";
			$data['data']		=	$detail;
			$data['stok']		=	$this->crud_model->select_all_where("stok", "id_produk", $detail->id_produk);
			$data['diskusi']	=	$this->crud_model->select_all_where_array("diskusi", ["id_produk" => $detail->id_produk, "reply_for" => null]);
		}

		$this->load->view("frontend/main", $data);
	}

	// tambah produk
	public function produk_tambah()
	{
		$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');
		if ($this->form_validation->run('produk') == FALSE) {
			// redirect("toko/produk_" . $method);
			$data['title']		=	"Tambah Produk";
			$data['page']		=	"toko/index";
			$data['subpage']	=	"produk_aksi";
			$data['method']		=	"Tambah";
			$data['kategori']	=	$this->crud_model->select_custom("select * from rb_kategori_produk where id_parent is null");

			$this->load->view("frontend/main", $data);
		} else {
			$data = [
				"id_produk" => $this->crud_model->cek_id("produk", "id_produk"),
				"id_penjual" => penjual("id_penjual"),
				"nama_produk" => $this->input->post("nama_produk", true),
				"produk_seo" => url_title($this->input->post("nama_produk", true), "-", TRUE),
				"id_kategori_produk" => $this->input->post("kategori", true),
				"satuan" => $this->input->post("satuan", true),
				"harga_konsumen" => $this->input->post("harga", true),
				"harga_nego" => $this->input->post("harga_nego", true),
				"diskon" => $this->input->post("diskon", true),
				"berat" => $this->input->post("berat", true),
				"kondisi" => $this->input->post("kondisi", true),
				"deskripsi" => $this->input->post("deskripsi", true),
				"status_produk" => "1"
			];

			$config['upload_path']     = './uploads/produk/';
			$config['allowed_types']   = 'jpg|png';
			$config['max_size']       	= 5000;
			$config['max_filename'] 	= '255';
			$config['encrypt_name'] 	= TRUE;

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('gambar')) {
				$data_file = $this->upload->data();
				$data['gambar']	=	$data_file['file_name'];
			} else {
				$data['gambar'] = "default.jpg";
			}
			// print_r($data);
			$tambah = $this->crud_model->insert("produk", $data);
			if ($tambah) {
				$this->crud_model->insert("stok", [
					"id_stok" => $this->crud_model->cek_id("stok", "id_stok"),
					"id_penjual" => penjual("id_penjual"),
					"id_produk" => $data["id_produk"],
					"stok" => $this->input->post("stok", ""),
					"keterangan" => "Stok Awal"
				]);
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Produk Berhasil Ditambah"
				);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Produk Gagal Ditambah"
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect("toko/produk");
		}
	}

	// edit produk
	public function produk_ubah($id_produk = null)
	{
		if ($id_produk === null) {
			redirect('frontend');
		} else {
			$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');
			if ($this->form_validation->run('produk') == FALSE) {
				// redirect("toko/produk_" . $method);
				$data['title']		=	"Ubah Produk";
				$data['page']		=	"toko/index";
				$data['subpage']	=	"produk_aksi";
				$data['method']		=	"Ubah";
				$data['kategori']	=	$this->crud_model->select_custom("select * from rb_kategori_produk where id_parent is null");
				$data['data']		=	$this->crud_model->select_one("produk", "id_produk", $id_produk);

				$this->load->view("frontend/main", $data);
			} else {
				$id_produk = $this->input->post("id_produk");
				$data = [
					"id_penjual" => penjual("id_penjual"),
					"nama_produk" => $this->input->post("nama_produk", true),
					"produk_seo" => url_title($this->input->post("nama_produk", true), "-", TRUE),
					"id_kategori_produk" => $this->input->post("kategori", true),
					"satuan" => $this->input->post("satuan", true),
					"harga_konsumen" => $this->input->post("harga", true),
					"harga_nego" => $this->input->post("harga_nego", true),
					"diskon" => $this->input->post("diskon", true),
					"berat" => $this->input->post("berat", true),
					"kondisi" => $this->input->post("kondisi", true),
					"deskripsi" => $this->input->post("deskripsi", true),
					"status_produk" => "1"
				];

				$config['upload_path']     = './uploads/produk/';
				$config['allowed_types']   = 'jpg|png';
				$config['max_size']       	= 5000;
				$config['max_filename'] 	= '255';
				$config['encrypt_name'] 	= TRUE;

				$detail = $this->crud_model->select_one("produk", "id_produk", $id_produk);
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('gambar')) {
					if ($detail->gambar != "default.jpg") {
						unlink('./uploads/produk/' . $detail->gambar);
					}
					$data_file = $this->upload->data();
					$data['gambar']	=	$data_file['file_name'];
				}
				// print_r($data);
				$ubah = $this->crud_model->update("produk", $data, "id_produk", $id_produk);
				if ($ubah) {
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Produk Berhasil Diubah"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Produk Gagal Diubah"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect("toko/produk");
			}
		}
	}

	public function produk_status($id_produk = null)
	{
		if ($id_produk === null) {
			redirect('frontend');
		} else {
			$data_update = [];
			$data = $this->crud_model->select_one("produk", "id_produk", $id_produk);
			if ($data->status_produk == "1") {
				$data_update['status_produk'] = "0";
			} else {
				$data_update['status_produk'] = "1";
			}
			$ubah = $this->crud_model->update("produk", $data_update, "id_produk", $id_produk);
			if ($ubah) {
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Status Berhasil Diubah"
				);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Status Gagal Diubah"
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect("toko/produk");
		}
	}

	function check_nama($nama_produk)
	{
		if ($this->input->post("method") == 'ubah') {
			$data = $this->crud_model->cek_data_where_array("produk", ["id_penjual" => penjual("id_penjual"), "nama_produk" => $nama_produk, "id_produk <>" => $this->input->post("id_produk")]);
		} else {
			$data = $this->crud_model->cek_data_where_array("produk", ["id_penjual" => penjual("id_penjual"), "nama_produk" => $nama_produk]);
		}
		if ($data) {
			return TRUE;
		} else {
			$this->form_validation->set_message('check_nama', $nama_produk . ' Sudah Pernah Anda Input');
			return FALSE;
		}
	}

	function check_default($post_string)
	{
		if ($post_string == '0') {
			$this->form_validation->set_message('check_default', '{field} Belum dipilih');
			return FALSE;
		} else {
			return TRUE;
		}
		//return $post_string == '0' ? FALSE : TRUE;
	}

	// cek file
	public function check_file()
	{
		$allowed_mime_type_arr = array('image/jpeg', 'image/pjpeg', 'image/x-citrix-jpeg', 'image/png', 'image/x-png', 'image/x-citrix-png');
		$mime = get_mime_by_extension($_FILES['gambar']['name']);
		if (isset($_FILES['gambar']['name']) && $_FILES['gambar']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				// return true;
				if ($_FILES['gambar']['size'] > 5242880) {
					$this->form_validation->set_message('check_file', 'Gambar terlalu besar. Maksimal 5MB');
					return false;
				} else {
					return true;
				}
			} else {
				$this->form_validation->set_message('check_file', 'Pilih file JPG atau PNG');
				return false;
			}
		} else {
			if ($this->input->post("method") == "tambah") {
				$this->form_validation->set_message('check_file', 'Pilih gambar produk.');
				return false;
			}
			return true;
		}
	}


	// =====================================================================================================================================
	// end produk
	// =====================================================================================================================================


	// =====================================================================================================================================
	// Stok
	// =====================================================================================================================================

	// tambah stok
	public function tambah_stok()
	{
		$detail = $this->crud_model->select_one("produk", "id_produk", $this->input->post("id_produk", ""));
		$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');
		if ($this->form_validation->run('stok') == FALSE) {
			$data['title']		=	"Detail Produk";
			$data['subtitle']	=	$detail->nama_produk;
			$data['page']		=	"toko/index";
			$data['subpage']	=	"produk_detail";
			$data['data']		=	$detail;
			$data['stok']		=	$this->crud_model->select_all_where("stok", "id_produk", $detail->id_produk);

			$this->load->view("frontend/main", $data);
		} else {
			$data = [
				"id_stok" => $this->crud_model->cek_id("stok", "id_stok"),
				"id_produk" => $this->input->post("id_produk", true),
				"id_penjual" => penjual("id_penjual"),
				"stok" => $this->input->post("stok", true),
				"keterangan" => $this->input->post("keterangan", true)
			];

			// print_r($data);
			$tambah = $this->crud_model->insert("stok", $data);
			if ($tambah) {
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Stok Berhasil Ditambah"
				);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Stok Gagal Ditambah"
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect("toko/produk/" . $detail->id_produk . "/" . $detail->produk_seo);
		}
	}

	// =====================================================================================================================================
	// End Stok
	// =====================================================================================================================================

	// =====================================================================================================================================
	// penjualan
	// =====================================================================================================================================

	public function penjualan($status = "menunggu")
	{
		$num_status = status_string_to_num($status);
		$where = ["id_penjual" => penjual("id_penjual"), "status" => $num_status];
		$data['title']		=	"Penjualan";
		$data['page']		=	"toko/index";
		$data['subpage']	=	"penjualan";
		// $data['data']		=	$this->crud_model->select_all_where("rb_penjualan", "id_penjual", penjual("id_penjual"));

		$data['status']		=	status_num_to_string($num_status);

		$this->load->library('pagination');

		$config['base_url'] = base_url() . "/penjualan/" . $data['status'];
		$config['total_rows'] = $this->crud_model->select_all_where_array_num_row("rb_penjualan", $where);
		$config['per_page'] = 10;
		$offset = ($this->uri->segment(3)  == 0) ? 0 : ($this->uri->segment(3) * $config['per_page']) - $config['per_page'];
		// $config["uri_segment"] = 3;  // uri parameter
		$order = ["key" => "waktu_transaksi", "value" => "DESC"];
		$penjualan		=	$this->crud_model->select_paging_where("rb_penjualan", $where, $config['per_page'], $offset, $order);

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();
		$data['penjualan'] = $penjualan;

		$this->load->view("frontend/main", $data);
	}

	// Detail penjualan
	public function penjualan_detail($kode = null)
	{
		if ($kode === null) {
			redirect("toko/saya");
		} else {
			$penjualan 			=	$this->crud_model->select_one("rb_penjualan", "kode_transaksi", $kode);
			if (empty($penjualan) || $penjualan->id_penjual != penjual("id_penjual")) {
				redirect();
			} else {
				$data['title']			=	"Detail Penjualan";
				$data['subtitle']		=	$penjualan->kode_transaksi;
				$data['page']			=	"toko/index";
				$data['subpage']		=	"penjualan_detail";
				$data['status']			=	status_num_to_string($penjualan->status);
				$data['data']			=	$penjualan;
				$data['detail']			=	$this->crud_model->select_all_where("rb_penjualan_detail", "id_penjualan", $penjualan->id_penjualan);
				$data['riwayat']		=	$this->crud_model->select_all_where_order("rb_penjualan_riwayat", "id_penjualan", $penjualan->id_penjualan, "create_at", "DESC");
				$data['pembeli']		=	$this->crud_model->select_one("users", "id_konsumen", $penjualan->id_pembeli);
				$data['lokasi_antar']	=	$this->crud_model->select_one("rb_desa", "desa_id", $penjualan->kelurahan_pembeli);

				$this->load->view("frontend/main", $data);
			}
		}
	}

	public function proses_pesanan($kode = null)
	{
		if ($kode === null) {
			redirect("toko/saya");
		} else {
			$penjualan 			=	$this->crud_model->select_one("rb_penjualan", "kode_transaksi", $kode);
			if (empty($penjualan)) {
				redirect("toko/penjualan");
			} else {
				if ($penjualan->status == 4) {
					redirect("penjualan/detail/" . $kode);
				} else {
					$penjual = $this->crud_model->select_one("penjual", "id_penjual", $penjualan->id_penjual);
					$pembeli = $this->crud_model->select_one("users", "id_konsumen", $penjualan->id_pembeli);
					$status = strval($penjualan->status + 1);
					$data = ["status" => $status];
					if ($this->crud_model->update("rb_penjualan", $data, "kode_transaksi", strtoupper($kode))) {
						if ($status == "2") {
							$id_produk = $this->crud_model->select_by_field("rb_penjualan_detail", "id_produk", ["id_penjualan" => $penjualan->id_penjualan]);
							foreach ($id_produk as $id_produk) {
								$this->crud_model->update("produk", ["terjual" => +1], "id_produk", $id_produk->id_produk);
							}
						}

						if ($status == "4") {
							$data_point = [
								"id_konsumen" => $penjualan->id_pembeli,
								"point" => $penjualan->total_produk / 100,
								"waktu_terima" => date("Y-m-d"),
								"waktu_ekspire" => date("Y-m-d", mktime(0, 0, 0, date("n"), date("j") + 90, date("Y")))
							];
							$this->crud_model->insert("point_get", $data_point);
						}

						if ($status == "2") {
							$status_msg = "Pesanan Anda Sedang Dikemas";
						} elseif ($status == "3") {
							$status_msg = "Pesanan Anda Sedang Diantar";
						} elseif ($status == "4") {
							$status_msg = "Pesanan Anda Telah Diterima";
						} elseif ($status == "0") {
							$status_msg = "Pesanan Anda Telah Ditolak oleh Penjual";
						}


						$this->crud_model->insert("rb_penjualan_riwayat", [
							"id_penjualan_riwayat"	=>	$this->crud_model->cek_id("rb_penjualan_riwayat", "id_penjualan_riwayat"),
							"id_penjualan"	=>	$penjualan->id_penjualan,
							"status"	=>	$status,
							"keterangan" => $status_msg
						]);

						// Kirim SMS
						$this->load->library('sms');
						$telp_pembeli = hp($pembeli->no_hp);
						$pesan = $status_msg . ". Dengan kode transaksi : " . $penjualan->kode_transaksi . ". Detail: " . base_url("pembelian/detail/" . $penjualan->kode_transaksi);
						$this->sms->kirimSms($telp_pembeli, $pesan);
						// Kirim SMS

						$data_notifikasi = [
							"tujuan" => $penjualan->id_pembeli,
							"dari"	=>	$penjual->nama_penjual,
							"judul" => "Pembelian",
							"pesan" => $status_msg,
							"link" => base_url("pembelian/detail/" . $penjualan->kode_transaksi),
							"status" => "0"
						];

						$this->crud_model->insert("notifikasi", $data_notifikasi);

						require APPPATH . 'views/vendor/autoload.php';

						$options = array(
							'cluster' => 'ap1',
							'useTLS' => true
						);
						$pusher = new Pusher\Pusher(
							'3c6926b288c9e03c843e',
							'5e63d5100e294b35d52d',
							'1138134',
							$options
						);

						$data['message'] = 'notifikasi';
						$pusher->trigger('my-channel', 'my-event', $data);

						$notifikasi		=	array(
							"status"	=>	"success", "msg"	=>	"Status penjualan diubah"
						);
					} else {
						$notifikasi		=	array(
							"status"	=>	"success", "msg"	=>	"Status penjualan gagal diubah"
						);
					}
					// print_r($data);
					$this->session->set_flashdata("notifikasi", $notifikasi);
					redirect("penjualan/detail/" . $kode);
				}
			}
		}
	}

	// =====================================================================================================================================
	// end penjualan
	// =====================================================================================================================================



	// =====================================================================================================================================
	// cod
	// =====================================================================================================================================

	public function cod()
	{
		$data['title']		=	"Ongkir Cash On Delivery (COD)";
		$data['page']		=	"toko/index";
		$data['subpage']	=	"cod";
		$data['data']		=	$this->crud_model->select_all_where("rb_reseller_cod", "id_reseller", penjual("id_penjual"));

		$this->load->view("frontend/main", $data);
	}

	// tambah cod
	public function cod_tambah()
	{
		$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');
		if ($this->form_validation->run('cod') == FALSE) {
			// redirect("toko/produk_" . $method);
			$data['title']		=	"Tambah Ongkir COD";
			$data['page']		=	"toko/index";
			$data['subpage']	=	"cod_aksi";
			$data['method']		=	"Tambah";
			$data['kota']		=	$this->crud_model->select_all("rb_kota");

			$this->load->view("frontend/main", $data);
		} else {
			$cek_kelurahan = $this->crud_model->select_one_where_array("rb_reseller_cod", ["desa_id" => $this->input->post("kelurahan", true), "id_reseller" => penjual("id_penjual")]);
			if (empty($cek_kelurahan)) {
				$data = [
					"id_cod" => $this->crud_model->cek_id("rb_reseller_cod", "id_cod"),
					"id_reseller" => penjual("id_penjual"),
					"kabupaten_id" => $this->input->post("kota_id", true),
					"kecamatan_id" => $this->input->post("kecamatan", true),
					"desa_id" => $this->input->post("kelurahan", true),
					"biaya_cod" => $this->input->post("harga", true)
				];

				// print_r($data);
				$aksi = $this->crud_model->insert("rb_reseller_cod", $data);
			} else {
				$data = [
					"biaya_cod" => $this->input->post("harga", true)
				];
				$aksi = $this->crud_model->update("rb_reseller_cod", $data, "id_cod", $cek_kelurahan->id_cod);
			}
			if ($aksi) {
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Ongkir COD Berhasil Ditambah"
				);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Ongkir COD Gagal Ditambah"
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect("toko/cod");
		}
	}

	// edit produk
	public function cod_ubah($id_cod = null)
	{
		if ($id_cod === null) {
			redirect('frontend');
		} else {
			$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');
			if ($this->form_validation->run('cod') == FALSE) {
				// redirect("toko/produk_" . $method);
				$data['title']		=	"Ubah Ongkir COD";
				$data['page']		=	"toko/index";
				$data['subpage']	=	"cod_aksi";
				$data['method']		=	"Ubah";
				$data['kota']		=	$this->crud_model->select_all("rb_kota");
				$data['data']		=	$this->crud_model->select_one("rb_reseller_cod", "id_cod", $id_cod);

				$this->load->view("frontend/main", $data);
			} else {
				$id_cod = $this->input->post("id_cod");
				$data = [
					"id_reseller" => penjual("id_penjual"),
					"kabupaten_id" => $this->input->post("kota_id", true),
					"kecamatan_id" => $this->input->post("kecamatan", true),
					"desa_id" => $this->input->post("kelurahan", true),
					"biaya_cod" => $this->input->post("harga", true)
				];

				// print_r($data);
				$ubah = $this->crud_model->update("rb_reseller_cod", $data, "id_cod", $id_cod);
				if ($ubah) {
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Ongkir COD Berhasil Diubah"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Ongkir COD Gagal Diubah"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect("toko/cod");
			}
		}
	}



	// =====================================================================================================================================
	// end cod
	// =====================================================================================================================================
}
