<?php
class Profil extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		if (!$this->session->userdata('user')) {
			$this->load->helper('url');
			$this->session->set_userdata('last_page', current_url());
			redirect('/login');
		}
	}

	public function index()
	{
		$data['title']		=	"Profil";
		$data['page']		=	"profil/index";
		$data['subpage']	=	"profil";
		$data['profil']		=	$this->crud_model->select_one("users", "id_konsumen", user("id_konsumen"));

		$this->load->view("frontend/main", $data);
	}

	public function point()
	{
		$data['title']		=	"Point";
		$data['page']		=	"profil/index";
		$data['subpage']	=	"point";
		$point_get = $this->crud_model->select_sum("point_get", "point", ["id_konsumen" => user("id_konsumen"), "waktu_ekspire >=" => date("Y-m-d")]);
		$point_claim = $this->crud_model->select_sum("point_claim", "point", ["id_konsumen" => user("id_konsumen")]);
		$data['point_saya']	=	$point_get - $point_claim;

		$this->load->view("frontend/main", $data);
	}

	public function edit()
	{
		$this->load->model("users_model");
		if ($this->input->post('simpan')) {
			$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'trim|required');
			$this->form_validation->set_rules('no_hp', 'No HP', 'trim|required|numeric|max_length[15]|min_length[11]');
			$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required');
			$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'trim|required');
			$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'trim|required|callback_check_date');
			$this->form_validation->set_rules('alamat_lengkap', 'Alamat Lengkap', 'trim|required');
			$this->form_validation->set_rules('kota_id', 'Kab / Kota', 'trim|required|callback_check_default');
			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required|callback_check_default');
			$this->form_validation->set_rules('kelurahan', 'Kelurahan', 'trim|required|callback_check_default');
			$this->form_validation->set_rules('foto', 'Foto', 'trim|callback_file_check');
			$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');
			if ($this->form_validation->run() == true) {
				$data = [
					"nama_lengkap" => $this->input->post("nama_lengkap", true),
					"no_hp" => $this->input->post("no_hp", true),
					"jenis_kelamin" => $this->input->post("jenis_kelamin", true),
					"tempat_lahir" => $this->input->post("tempat_lahir", true),
					"tanggal_lahir" => $this->input->post("tanggal_lahir", true),
					"alamat_lengkap" => $this->input->post("alamat_lengkap", true),
					"kota_id" => $this->input->post("kota_id", true),
					"kecamatan" => $this->input->post("kecamatan", true),
					"kelurahan" => $this->input->post("kelurahan", true)
				];

				$config['upload_path']     = './uploads/users/';
				$config['allowed_types']   = 'jpg|png';
				$config['max_size']       	= 2000;
				$config['max_filename'] 	= '255';
				$config['encrypt_name'] 	= TRUE;

				$detail		=	$this->crud_model->select_one("users", "id_konsumen", user("id_konsumen"));
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('foto')) {
					$data_file = $this->upload->data();
					$data['foto']	=	$data_file['file_name'];
					if ($detail->foto != "default.jpg") {
						unlink('./uploads/users/' . $detail->foto);
					}
				}

				$simpan		=	$this->crud_model->update("users", $data, "id_konsumen", user("id_konsumen"));
				if ($simpan) {
					$session = $this->users_model->get_data_after_update(user("id_konsumen"));
					$this->session->unset_userdata('user');
					$this->session->set_userdata('user', $session);
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Profil Berhasil Diperbarui"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Profil Gagal Diperbarui"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect("profil");
			} else {
				$data['title']		=	"Edit Profil";
				$data['page']		=	"profil/index";
				$data['subpage']	=	"edit";
				$data['data']		=	$this->crud_model->select_one("users", "id_konsumen", user("id_konsumen"));
				$data['kota']		=	$this->crud_model->select_all("rb_kota");

				$this->load->view("frontend/main", $data);
			}
		} else {
			$data['title']		=	"Edit Profil";
			$data['page']		=	"profil/index";
			$data['subpage']	=	"edit";
			$data['data']		=	$this->crud_model->select_one("users", "id_konsumen", user("id_konsumen"));
			$data['kota']		=	$this->crud_model->select_all("rb_kota");

			$this->load->view("frontend/main", $data);
		}
	}

	// ganti katasandi
	public function ganti_katasandi()
	{
		$this->load->model("users_model");
		if ($this->input->post('simpan')) {
			$this->form_validation->set_rules('pass_lama', 'Password Lama', 'trim|required|callback_passlama');
			$this->form_validation->set_rules('pass_baru', 'Password Baru', 'trim|required|min_length[8]');
			$this->form_validation->set_rules('pass_baru2', 'Ulangi Password Baru', 'trim|required|matches[pass_baru]');
			$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');
			if ($this->form_validation->run() == true) {
				$data = [
					"password"		=>	hash("sha512", md5($this->input->post("pass_baru", true)))
				];

				$simpan		=	$this->crud_model->update("users", $data, "id_konsumen", user("id_konsumen"));
				if ($simpan) {
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Password Berhasil Diperbarui"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Password Gagal Diperbarui"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect("profil");
			} else {
				$data['title']		=	"Ganti Katasandi";
				$data['page']		=	"profil/index";
				$data['subpage']	=	"ganti_password";
				$data['data']		=	$this->crud_model->select_one("users", "id_konsumen", user("id_konsumen"));
				$this->load->view("frontend/main", $data);
			}
		} else {
			$data['title']		=	"Ganti Katasandi";
			$data['page']		=	"profil/index";
			$data['subpage']	=	"ganti_password";
			$data['data']		=	$this->crud_model->select_one("users", "id_konsumen", user("id_konsumen"));
			$this->load->view("frontend/main", $data);
		}
	}

	// Daftar pembelian
	public function pembelian($status = "menunggu")
	{
		$num_status = status_string_to_num($status);
		$where = ["id_pembeli" => user("id_konsumen"), "status" => $num_status];
		$data['title']		=	"Pembelian";
		$data['page']		=	"profil/index";
		$data['subpage']	=	"pembelian";
		// $data['status']		=	$status;
		$data['status']		=	status_num_to_string($num_status);

		$this->load->library('pagination');

		$config['base_url'] = base_url() . "/pembelian/" . $data['status'];
		$config['total_rows'] = $this->crud_model->select_all_where_array_num_row("rb_penjualan", $where);
		$config['per_page'] = 10;
		$offset = ($this->uri->segment(3)  == 0) ? 0 : ($this->uri->segment(3) * $config['per_page']) - $config['per_page'];
		// $config["uri_segment"] = 3;  // uri parameter
		$order = ["key" => "waktu_transaksi", "value" => "DESC"];
		$pembelian		=	$this->crud_model->select_paging_where("rb_penjualan", $where, $config['per_page'], $offset, $order);

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();
		$data['pembelian'] = $pembelian;
		$this->load->view("frontend/main", $data);
		// echo $this->uri->segment(3);
	}

	// Detail pembelian
	public function pembelian_detail($kode = null)
	{
		if ($kode === null) {
			redirect("profil/saya");
		} else {
			$pembelian 			=	$this->crud_model->select_one("rb_penjualan", "kode_transaksi", $kode);
			if (empty($pembelian) || $pembelian->id_pembeli != user("id_konsumen")) {
				redirect("profil/pembelian");
			} else {
				$data['title']			=	"Detail Pembelian";
				$data['subtitle']		=	$pembelian->kode_transaksi;
				$data['page']			=	"profil/index";
				$data['subpage']		=	"pembelian_detail";
				$data['status']			=	status_num_to_string($pembelian->status);
				$data['data']			=	$pembelian;
				$data['detail']			=	$this->crud_model->select_all_where("rb_penjualan_detail", "id_penjualan", $pembelian->id_penjualan);
				$data['riwayat']		=	$this->crud_model->select_all_where_order("rb_penjualan_riwayat", "id_penjualan", $pembelian->id_penjualan, "create_at", "DESC");
				$data['penjual']		=	$this->crud_model->select_one("penjual", "id_penjual", $pembelian->id_penjual);
				$data['lokasi_antar']	=	$this->crud_model->select_one("rb_desa", "desa_id", $pembelian->kelurahan_pembeli);

				$this->load->view("frontend/main", $data);
			}
			// print_r($data);
		}
	}


	// mendaftar segabai Penjual
	public function daftar_penjual()
	{
		$this->load->model("users_model");
		if ($this->input->post('simpan')) {
			if ($this->input->post("klaster_pasar")) {
				// if($this->input->post())
				$this->form_validation->set_rules(
					'pasar',
					'Pasar',
					'trim|required|callback_check_default',
					array('required' => 'Jika anda mencentang penjual pasar. Harus memilih salah satu pasar.')
				);
			}

			$this->form_validation->set_rules('nama_penjual', 'Nama Penjual', 'trim|required');
			$this->form_validation->set_rules('no_telpon', 'No Handphone', 'trim|required|numeric|max_length[15]|min_length[11]');
			$this->form_validation->set_rules('alamat_lengkap', 'Alamat Lengkap', 'trim|required');
			$this->form_validation->set_rules('kota_id', 'Kab / Kota', 'trim|required|callback_check_default');
			$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'trim|required|callback_check_default');
			$this->form_validation->set_rules('kelurahan', 'Kelurahan', 'trim|required|callback_check_default');
			$this->form_validation->set_rules('foto', 'Foto', 'trim|callback_foto_check');
			$this->form_validation->set_rules('ktp', 'KTP', 'trim|callback_ktp_check');
			$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');
			if ($this->form_validation->run() == true) {
				$cek_nama_sama = $this->crud_model->cek_data("penjual", "penjual_seo", strtolower(url_title($this->input->post("nama_penjual", true))));
				if ($cek_nama_sama) {
					$penjual_seo = strtolower(url_title($this->input->post("nama_penjual", true)));
				} else {
					$penjual_seo = strtolower(url_title($this->input->post("nama_penjual", true) . "-" . rand(1, 99)));
				}
				// $data = [];
				$data = [
					"id_penjual" => $this->crud_model->cek_id("penjual", "id_penjual"),
					"id_user" => user("id_konsumen"),
					"nama_penjual" => $this->input->post("nama_penjual", true),
					"penjual_seo" => $penjual_seo,
					"no_telpon" => $this->input->post("no_telpon", true),
					"alamat_lengkap" => $this->input->post("alamat_lengkap", true),
					"kota_id" => $this->input->post("kota_id", true),
					"kecamatan" => $this->input->post("kecamatan", true),
					"kelurahan" => $this->input->post("kelurahan", true)
				];
				if ($this->input->post("klaster_pasar")) {
					$data["klaster_pasar"] = "1";
					$data["id_pasar"] = $this->input->post("pasar", true);
				}

				if (isset($_FILES['foto']['name']) && $_FILES['foto']['name'] != "") {
					$config['upload_path']     = './uploads/users/';
					$config['allowed_types']   = 'jpg|png';
					$config['max_size']       	= 2000;
					$config['max_filename'] 	= '255';
					$config['encrypt_name'] 	= TRUE;

					$this->load->library('upload', $config);
					if ($this->upload->do_upload('foto')) {
						$data_file = $this->upload->data();
						$data['foto']	=	$data_file['file_name'];
					}
				}

				if (isset($_FILES['ktp']['name']) && $_FILES['ktp']['name'] != "") {
					unset($config);

					$config['upload_path']     = './uploads/ktp_penjual/';
					$config['allowed_types']   = 'jpg|png';
					$config['max_size']       	= 2000;
					$config['max_filename'] 	= '255';
					$config['encrypt_name'] 	= TRUE;

					// $this->load->library('upload', $config);
					$this->upload->initialize($config);
					if ($this->upload->do_upload('ktp')) {
						$data_file = $this->upload->data();
						$data['ktp_penjual']	=	$data_file['file_name'];
					}
				}

				$simpan		=	$this->crud_model->insert("penjual", $data);
				if ($simpan) {
					$session = $this->users_model->get_data_penjual_after_update($data["id_penjual"]);
					$this->session->set_userdata('penjual', $session);
					$this->crud_model->update("users", ["penjual" => "1"], "id_konsumen", user("id_konsumen"));
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Berhasil terdaftar sebagai penjual. Toko anda akan diverifikasi oleh admin untuk mendapatkan label terverifikasi"
					);
					$this->session->set_flashdata("notifikasi", $notifikasi);
					redirect("toko/saya");
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Gagal terdaftar sebagai penjual"
					);
					$this->session->set_flashdata("notifikasi", $notifikasi);
					redirect("profil/saya");
				}
			} else {
				$data['title']		=	"Mendaftar Sebagai Penjual";
				$data['page']		=	"profil/index";
				$data['subpage']	=	"daftar_penjual";
				$data['kota']		=	$this->crud_model->select_all("rb_kota");

				$this->load->view("frontend/main", $data);
			}
		} else {
			$data['title']		=	"Mendaftar Sebagai Penjual";
			$data['page']		=	"profil/index";
			$data['subpage']	=	"daftar_penjual";
			$data['kota']		=	$this->crud_model->select_all("rb_kota");

			$this->load->view("frontend/main", $data);
		}
	}

	// cek file upload
	public function file_check($str)
	{
		$allowed_mime_type_arr = array('image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png');
		$mime = get_mime_by_extension($_FILES['foto']['name']);
		if (isset($_FILES['foto']['name']) && $_FILES['foto']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				return true;
			} else {
				$this->form_validation->set_message('file_check', 'Pilih file JPG atau PNG');
				return false;
			}
		} else {
			// $this->form_validation->set_message('file_check', 'Foto Belum Dipilih');
			return true;
		}
	}

	public function foto_check($str)
	{
		$allowed_mime_type_arr = array('image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png');
		$mime = get_mime_by_extension($_FILES['foto']['name']);
		if (isset($_FILES['foto']['name']) && $_FILES['foto']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				return true;
			} else {
				$this->form_validation->set_message('file_check', 'Pilih file JPG atau PNG');
				return false;
			}
		} else {
			$this->form_validation->set_message('file_check', 'Foto Belum Dipilih');
			return false;
		}
	}

	// cek file ktp
	public function ktp_check($str)
	{
		$allowed_mime_type_arr = array('image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png');
		$mime = get_mime_by_extension($_FILES['ktp']['name']);
		if (isset($_FILES['ktp']['name']) && $_FILES['ktp']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				return true;
			} else {
				$this->form_validation->set_message('ktp_check', 'Pilih file JPG atau PNG');
				return false;
			}
		} else {
			$this->form_validation->set_message('ktp_check', 'KTP Tidak Boleh Kosong');
			return false;
		}
	}

	function check_default($post_string)
	{
		if ($post_string == '0') {
			$this->form_validation->set_message('check_default', '{field} Belum dipilih');
			return FALSE;
		} else {
			return TRUE;
		}
		//return $post_string == '0' ? FALSE : TRUE;
	}

	function check_date($post_string)
	{
		if ($post_string == '0000-00-00') {
			$this->form_validation->set_message('check_date', '{field} Tidak valid');
			return FALSE;
		} else {
			return TRUE;
		}
		//return $post_string == '0' ? FALSE : TRUE;
	}

	public function passlama($str)
	{
		$where	=	array(
			"password"			=>	hash("sha512", md5($str))
		);
		$cek	=	$this->crud_model->cek_data_where_array("users", $where);
		if ($cek) {
			$this->form_validation->set_message('passlama', 'Password Lama Salah');
			return FALSE;
		} else {
			return TRUE;
		}
	}
}
