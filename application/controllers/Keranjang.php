<?php
class Keranjang extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		// if ($this->session->userdata('user')) {
		// 	$this->session->set_userdata('last_page', current_url());
		// 	redirect('/login');
		// }
	}

	// tambah ke keranjang
	public function index($id_produk)
	{
		if ($id_produk === null) {
			echo "kosong";
			// redirect("e404");
		} else {
			if (!$this->session->userdata('user')) {
				$this->session->set_userdata('last_page', current_url());
				$this->session->set_userdata('form_keranjang', $this->input->post());
				redirect('login');
			} else {
				// print_r($this->session->userdata());
				$data_pembeli = ["id_pembeli" => user("id_konsumen")];
				if ($this->session->userdata("form_keranjang")) {
					$data = array_merge($this->session->userdata("form_keranjang"), $data_pembeli);
					$this->session->unset_userdata("form_keranjang");
				} else {
					$data = array_merge($data_pembeli, [
						"id_penjual" => $this->input->post("id_penjual", true),
						"id_produk" => $this->input->post("id_produk", true),
						"diskon" => $this->input->post("diskon", true),
						"harga_asli" => $this->input->post("harga_asli", true),
						"harga" => $this->input->post("harga", true),
						"qty" => $this->input->post("qty", true),
						"total" => $this->input->post("total", true),
					]);
				}
				// print_r($data);
				if (penjual("id_penjual") && (penjual("id_penjual") == $data["id_penjual"])) {
					$this->session->set_flashdata("notifikasi", ["status" => "warning", "msg" => "Tidak dapat memesan barang dari toko sendiri"]);
					redirect("keranjang/saya");
				}

				$cek_keranjang = $this->crud_model->select_one("keranjang", "id_pembeli", user("id_konsumen"));
				if ($cek_keranjang) {
					if ($cek_keranjang->id_penjual != $data["id_penjual"]) {
						$this->session->set_flashdata("notifikasi", ["status" => "warning", "msg" => "Hanya dapat memesan dari satu penjual"]);
						redirect("keranjang/saya");
					}
				}

				$cek_existing = $this->crud_model->cek_data_where_array("keranjang", ["id_produk" => $data["id_produk"], "id_pembeli" => $data["id_pembeli"]]);
				if ($cek_existing) {
					$simpan = $this->crud_model->insert("keranjang", $data);
				} else {
					$data_exist = $this->crud_model->select_one_where_array("keranjang", ["id_produk" => $data["id_produk"], "id_pembeli" => $data["id_pembeli"]]);
					$stok_tmp	=	$this->crud_model->select_sum("stok", "stok", ["id_produk" => $data_exist->id_produk]);
					$terjual	=	$this->crud_model->select_sum("rb_penjualan_detail", "qty", ["id_produk" => $data_exist->id_produk, "status >" => 0], ["table" => "rb_penjualan", "key" => "id_penjualan"]);

					$stok		=	$stok_tmp - $terjual - ($data_exist->qty + $data["qty"]);
					if ($stok > 0) {
						$simpan = $this->crud_model->update_where_array("keranjang", ["qty" => $data_exist->qty + $data["qty"], "total" => $data_exist->total + $data["total"]], ["id_produk" => $data["id_produk"], "id_pembeli" => $data["id_pembeli"]]);
						echo $stok;
					} else {
						$this->session->set_flashdata("notifikasi", ["status" => "danger", "msg" => "Stok tidak cukup"]);
						redirect("keranjang/saya");
					}
				}
				if ($simpan) {
					redirect("keranjang/saya");
				} else {
					redirect("frontend");
				}
			}
		}
	}

	// keranjang saya
	public function saya()
	{
		if (!$this->session->userdata("user")) {
			redirect("frontend");
		} else {
			$this->load->model("join_model");
			$rowKeranjang = $this->crud_model->select_one("keranjang", "id_pembeli", user("id_konsumen"));
			$data["keranjang"] = $this->crud_model->select_all_where("keranjang", "id_pembeli", user("id_konsumen"));
			if ($rowKeranjang) {
				$data["penjual"] = $this->join_model->dua_tabel_where_array_row("keranjang", "penjual", "id_penjual", ["keranjang.id_penjual" => $rowKeranjang->id_penjual]);
				$data["produk_penjual"] = $this->crud_model->select_all_where_array_limit("produk", ["id_penjual" => $rowKeranjang->id_penjual], "hits", "DESC", 6);
			}
			// $data['kota'] = $this->crud_model->select_all_where("rb_kota", "kota_id", "129");
			$data['kota'] = $this->crud_model->select_all("rb_kota");
			$data['pembeli'] = $this->crud_model->select_one("users", "id_konsumen", user("id_konsumen"));
			$data['title'] = "Keranjang Belanja";
			$data['page'] = "/keranjang";
			// print_r($data);
			$this->load->view("frontend/main", $data);
		}
	}

	// ubah jumlah beli dari keranjang
	public function gantiJumlah()
	{
		$ret = array();
		if (!$this->input->post("id_keranjang", true)) {
			$ret = ["status" => 0, "pesan" => "id tidak dikenali"];
		} else {
			$data = [
				"qty" => $this->input->post("qty", true),
				"total" => $this->input->post("total", true)
			];
			$ubah = $this->crud_model->update("keranjang", $data, "id_keranjang", $this->input->post("id_keranjang"));
			if ($ubah) {
				$ret = ["status" => "1", "pesan" => "jumlah beli diperbarui"];
			} else {
				$ret = ["status" => "0", "pesan" => "jumlah beli gagal diperbarui"];
			}
		}
		echo json_encode($ret);
	}

	// hapus dari keranjang
	public function hapus()
	{
		$ret = array();
		if (!$this->input->post("id_keranjang")) {
			$ret = ["status" => 0, "pesan" => "id tidak dikenali"];
		} else {
			$hapus = $this->crud_model->hapus_id("keranjang", "id_keranjang", $this->input->post("id_keranjang"));
			if ($hapus) {
				$ret = ["status" => "1", "pesan" => "item di hapus dari keranjang"];
			} else {
				$ret = ["status" => "0", "pesan" => "item di gagal hapus dari keranjang"];
			}
		}
		echo json_encode($ret);
	}

	// checkout
	public function checkout()
	{
		$this->load->model("join_model");
		if (!$this->session->userdata("user")) {
			redirect("frontend");
		} else {
			$cek_keranjang = $this->crud_model->cek_data("keranjang", "id_pembeli", user("id_konsumen"));
			if (!$cek_keranjang || $this->input->post("keterangan")) {
				$temporary = [
					"keterangan" => $this->input->post("keterangan", true),
					"alamat" => $this->input->post("alamat", true),
					"kota_id" => $this->input->post("kota_id", true),
					"kecamatan" => $this->input->post("kecamatan", true),
					"kelurahan" => $this->input->post("kelurahan", true)
				];
				$this->session->set_userdata("temporary", $temporary);
				$rowKeranjang = $this->crud_model->select_one("keranjang", "id_pembeli", user("id_konsumen"));
				$penjual = $this->join_model->dua_tabel_where_array_row("keranjang", "penjual", "id_penjual", ["keranjang.id_penjual" => $rowKeranjang->id_penjual]);

				$penjual_cod = $this->crud_model->select_one_where_array("rb_reseller_cod", ["id_reseller" => $penjual->id_penjual, "desa_id" => $temporary['kelurahan']]);
				if ($penjual_cod) {
					$data['ongkir'] = $penjual_cod->biaya_cod;
				} else {
					$admin = $this->crud_model->select_one_where_array("cod_admin", ["kelurahan_penjual" => $penjual->kelurahan, "kelurahan_pembeli" => $temporary['kelurahan']]);
					$data['ongkir'] = $admin->biaya_cod;
				}

				// Promo
				if ($this->input->post('kode_promo', true) != "") {
					$jumBelanja	=	$this->crud_model->select_sum('keranjang', 'total', ["id_pembeli" => user("id_konsumen")]);
					$cek_data	=	$this->crud_model->select_one_where_array('promo', [
						// "kuota >" => 0,
						"kode_referal"	=>	$this->input->post('kode_promo', true)
					]);
					if (empty($cek_data)) {
						$temporary['kode_promo']	=	$this->input->post('kode_promo', true);
						$temporary['status_promo']	=	'0';
						$temporary['msg_promo']		=	'Kode Promo tidak dikenali';
					} else {
						if ($cek_data->kuota == "0") {
							$temporary['kode_promo']	=	$this->input->post('kode_promo', true);
							$temporary['jenis_promo']	=	"0";
							$temporary['status_promo']	=	'0';
							$temporary['msg_promo']		=	"Promo sudah habis";
						} elseif ($cek_data->minimal_belanja <= $jumBelanja) {
							$temporary['kode_promo']	=	$this->input->post('kode_promo', true);
							$temporary['status_promo']	=	'1';
							$temporary['jenis_promo']	=	$cek_data->jenis;
							if ($cek_data->jenis == "ongkir") {
								$potongan = ($cek_data->potongan / 100) * $data['ongkir'];
								if ($cek_data->maksimal_subsidi < $potongan) {
									$potongan = $cek_data->maksimal_subsidi;
								}
								$temporary['maksimal_promo']	=	$cek_data->maksimal_subsidi;
								$temporary['potongan_ongkir']	=	$potongan;
								$temporary['potongan_barang']	=	0;
							} else {
								$potongan = ($cek_data->potongan / 100) * $jumBelanja;
								if ($cek_data->maksimal_subsidi < $potongan) {
									$potongan = $cek_data->maksimal_subsidi;
								}
								$temporary['maksimal_promo']	=	$cek_data->maksimal_subsidi;
								$temporary['potongan_ongkir']	=	0;
								$temporary['potongan_barang']	=	$potongan;
							}
							$temporary['msg_promo']		=	$cek_data->keterangan;
						} else {
							$temporary['kode_promo']	=	$this->input->post('kode_promo', true);
							$temporary['jenis_promo']	=	"0";
							$temporary['status_promo']	=	'0';
							$temporary['msg_promo']		=	"Tidak mencapai minimal belanja. (Minimal belanja " . rupiah($cek_data->minimal_belanja) . ")";
						}
					}
				} else {
					$temporary['status_promo']	=	'2';
					$temporary['jenis_promo']	=	'0';
				}
				$this->session->set_userdata("temporary", $temporary);
				// end promo

				$data["keranjang"] = $this->crud_model->select_all_where("keranjang", "id_pembeli", user("id_konsumen"));
				$data["penjual"] = $penjual;

				$data['pembeli'] = $this->crud_model->select_one("users", "id_konsumen", user("id_konsumen"));
				$data['title'] = "Checkout";
				$data['page'] = "/checkout";
				$this->load->view("frontend/main", $data);
			} else {
				// $this->session->set_flashdata("notifikasi", ["status" => "danger", "msg" => "Keranjang Anda Telah Kosong"]);
				redirect("keranjang/saya");
			}
		}
	}

	// buat pesanan
	public function buat_pesanan()
	{
		$this->load->model("join_model");
		if (!$this->session->userdata("user")) {
			redirect("frontend");
		} else {
			$cek_keranjang = $this->crud_model->cek_data("keranjang", "id_pembeli", user("id_konsumen"));
			if (!$cek_keranjang) {
				$rowKeranjang = $this->crud_model->select_one("keranjang", "id_pembeli", user("id_konsumen"));
				$penjual = $this->join_model->dua_tabel_where_array_row("keranjang", "penjual", "id_penjual", ["keranjang.id_penjual" => $rowKeranjang->id_penjual]);

				$penjual_cod = $this->crud_model->select_one_where_array("rb_reseller_cod", ["id_reseller" => $penjual->id_penjual, "desa_id" => $this->session->userdata("temporary")['kelurahan']]);
				if ($penjual_cod) {
					$ongkir = $penjual_cod->biaya_cod;
				} else {
					$admin = $this->crud_model->select_one_where_array("cod_admin", ["kelurahan_penjual" => $penjual->kelurahan, "kelurahan_pembeli" => $this->session->userdata("temporary")['kelurahan']]);
					$ongkir = $admin->biaya_cod;
				}
				$id_penjualan = $this->crud_model->cek_id("rb_penjualan", "id_penjualan");
				$total_produk = [];
				$keranjang = $this->crud_model->select_all_where("keranjang", "id_pembeli", user("id_konsumen"));
				$detail_penjualan = [];
				$stok = [];
				foreach ($keranjang as $k) {

					$stok_tmp	=	$this->crud_model->select_sum("stok", "stok", ["id_produk" => $k->id_produk]);
					$terjual	=	$this->crud_model->select_sum("rb_penjualan_detail", "qty", ["id_produk" => $k->id_produk, "status >" => 0], ["table" => "rb_penjualan", "key" => "id_penjualan"]);

					$stok[] 	=	$stok_tmp - $terjual - $k->qty;

					$detail_penjualan[] = [
						"id_penjualan" => $id_penjualan,
						"id_produk" => $k->id_produk,
						"qty" => $k->qty,
						"harga_asli" => $k->harga_asli,
						"diskon" => $k->diskon,
						"harga" => $k->harga,
						"total" => $k->total
					];
					$total_produk[] = $k->total;
				}

				$cek_stok = array_reduce($stok, function ($isBigger, $num) {
					return $isBigger || $num < 0;
				});
				if ($cek_stok) {
					// echo "ada stok yang kosong";
					$this->session->set_flashdata("notifikasi", ["status" => "danger", "msg" => "Stok barang yang anda pesan telah habis. Jangan terlalu lama menyimpan barang di keranjang belanja. Agar tidak di dahului pembeli lain."]);
					redirect("keranjang/saya");
				} else {


					$data = [
						"id_penjualan" => $id_penjualan,
						"kode_transaksi" => "SIP" . date("ymd") . rand(11, 99) . $id_penjualan,
						"id_pembeli" => user("id_konsumen"),
						"id_penjual" => $penjual->id_penjual,
						"ongkir" => $ongkir,
						"total_produk" => array_sum($total_produk),
						"keterangan_pembeli" => temp("keterangan"),
						"alamat_pembeli" => temp("alamat"),
						"kelurahan_pembeli" => temp("kelurahan")
					];

					$data['potongan_ongkir']	= 0;
					if (temp('status_promo') == "1" && temp('jenis_promo') == "ongkir") {
						$data['kode_promo']			= temp('kode_promo');
						$data['potongan_ongkir']	= temp('potongan_ongkir');
					}

					$data['potongan_barang']	= 0;
					if (temp('status_promo') == "1" && temp('jenis_promo') == "produk") {
						$data['kode_promo']			= temp('kode_promo');
						$data['potongan_barang']	= temp('potongan_barang');
					}

					$data_notifikasi = [
						"tujuan" => $penjual->id_user,
						"dari"	=>	user("nama_lengkap"),
						"judul" => "Pembelian",
						"pesan" => user("nama_lengkap") . " Telah memesan barang pada toko anda.",
						"link" => base_url("penjualan/detail/" . $data["kode_transaksi"]),
						"status" => "0"
					];
					$email_pembeli = ambil_nama_by_id("users", "email", "id_konsumen", user("id_konsumen"));
					$email_penjual = ambil_nama_by_id("users", "email", "id_konsumen", $penjual->id_user);
					$this->load->model("email_model");
					$tujuan_pembeli = $email_pembeli;
					$judul_pembeli = "Pembelian";
					$content_pembeli = template_pesanan($data, $detail_penjualan, "pembeli");
					$tujuan_penjual = $email_penjual;
					$judul_penjual = "Pemesanan";
					$content_penjual = template_pesanan($data, $detail_penjualan, "penjual");
					$simpan = $this->crud_model->insert("rb_penjualan", $data);
					if ($simpan) {
						$this->email_model->kirim_email($tujuan_pembeli, $judul_pembeli, $content_pembeli);
						$this->email_model->kirim_email($tujuan_penjual, $judul_penjual, $content_penjual);

						// Kirim SMS
						$this->load->library('sms');
						$telp_penjual = hp($penjual->no_telpon);
						$pesan = "Ada pesanan baru dengan kode transaksi : " . $data["kode_transaksi"] . ". Detail: " . base_url("penjualan/detail/" . $data["kode_transaksi"]);
						$this->sms->kirimSms($telp_penjual, $pesan);
						// Kirim SMS

						$this->crud_model->insert("rb_penjualan_riwayat", [
							"id_penjualan_riwayat"	=>	$this->crud_model->cek_id("rb_penjualan_riwayat", "id_penjualan_riwayat"),
							"id_penjualan"	=>	$data["id_penjualan"],
							"status"	=>	"1",
							"keterangan" =>	"Pesanan Dibuat"
						]);

						$this->session->set_flashdata("notifikasi", ["status" => "success", "msg" => "Transaksi berhasil di proses"]);
						$this->crud_model->insert_batch("rb_penjualan_detail", $detail_penjualan);
						$this->crud_model->insert("notifikasi", $data_notifikasi);

						require APPPATH . 'views/vendor/autoload.php';

						$options = array(
							'cluster' => 'ap1',
							'useTLS' => true
						);
						$pusher = new Pusher\Pusher(
							'3c6926b288c9e03c843e',
							'5e63d5100e294b35d52d',
							'1138134',
							$options
						);

						$data['message'] = 'notifikasi';
						$pusher->trigger('my-channel', 'my-event', $data);
					} else {
						$this->session->set_flashdata("notifikasi", ["status" => "danger", "msg" => "Transaksi gagal di proses"]);
					}
					$this->crud_model->hapus_id("keranjang", "id_pembeli", user("id_konsumen"));
					$this->session->unset_userdata("temporary");
					redirect("profil/pembelian/menunggu");
				}
			} else {
				redirect("keranjang/saya");
			}
		}
	}
}
