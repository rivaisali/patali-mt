<?php
class Blog extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
    }

    public function index()
    {
        $data['title']        =    "Blog";
        $data['data']        =    $this->crud_model->select_all_where_order('berita', 'aktif', 'Y', 'tanggal', 'DESC', 6);
        $data['page']        =    "index";
        $this->load->view("frontend/blog", $data);
    }

    public function baca($seo = null)
    {
        if ($seo == null) {
            redirect('blog');
        } else {
            $blog               =   $this->crud_model->select_one('berita', 'judul_seo', $seo);
            $this->crud_model->update('berita', ['dibaca' => $blog->dibaca + 1], 'id_berita', $blog->id_berita);
            $data['title']      =   $blog->judul;
            $data['data']       =   $blog;
            $data['others']     =   $this->crud_model->select_all_where_array_order('berita', ['id_berita <>' => $blog->id_berita, 'aktif' => 'Y'], 'tanggal', 'DESC', 5);
            $data['page']       =   "baca";
            $this->load->view("frontend/blog", $data);
        }
    }
}
