<?php
class Register extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
	}

	public $base	= 'frontend';
	public $konten	= '/frontend';

	public function index()
	{
		if ($this->session->userdata('user')) {
			redirect('frontend');
		} else {
			if ($this->input->post('daftar')) {
				$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
				$this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|required|is_unique[users.email]');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|max_length[30]');
				$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');

				if ($this->form_validation->run() == FALSE) {
					$data['title']		=	"Mendaftar di Sipardi";
					$this->load->view("frontend/register", $data);
				} else {
					$data	=	array(
						"id_konsumen"	=>	$this->crud_model->cek_id("users", "id_konsumen"),
						"password"		=>	hash("sha512", md5($this->input->post("password", true))),
						"nama_lengkap"	=>	$this->input->post("nama", true),
						"email"			=>	$this->input->post("email", true),
						"status_user"	=>	"0"
					);
					$daftar		=	$this->crud_model->insert("users", $data);
					if ($daftar) {
						$verifikasi_email = [
							"id_konsumen" => $data["id_konsumen"],
							"kode_referal" => generateRandomString(30)
						];
						$this->crud_model->insert("verifikasi_email", $verifikasi_email);
						$this->load->model("email_model");
						$tujuan = $this->input->post("email", true);
						$judul = "Verifikasi Email Anda";
						$content = 'Selamat ' . $data["nama_lengkap"] . '!. Anda berhasil mendaftar di SIPARDI. Silhakan klik link berikut ini.
						<a href="' . base_url("verifikasi/" . $verifikasi_email["kode_referal"]) . '"> Klik disini </a>untuk melakukan verifikasi email anda.';
						$this->email_model->kirim_email($tujuan, $judul, $content);
						$notifikasi		=	array(
							"status"	=>	1, "pesan"	=>	"Anda berhasil mendaftar, Silahkan lihat email anda untuk verifikasi email."
						);
					} else {
						$notifikasi		=	array(
							"status"	=>	0, "pesan"	=>	"Gagal Mendaftar"
						);
					}
					$this->session->set_flashdata("notifikasi", $notifikasi);
					redirect("register");
				}
			} else {
				$data['title']		=	"Mendaftar di Sipardi";
				$this->load->view("frontend/register", $data);
			}
		}
	}
}
