<?php
defined('BASEPATH') or exit('No direct script access allowed');
$route['default_controller'] = 'frontend';
$route['404_override'] = 'e404';
$route['translate_uri_dashes'] = true;

$route['/'] = 'frontend/index';
$route['e404'] = 'e404/index';
$route['frontend'] = 'frontend/index';
$route['testing'] = 'testing/index';
$route['login'] = 'login';
$route['login/do_login'] = 'login/do_login';
$route['login/logout'] = 'login/logout';
$route['logout'] = 'login/logout';
$route['forget'] = 'forget';
$route['register'] = 'register';
// rute verifikasi email pendaftar
$route['verifikasi/(:any)'] = 'Frontend/verifikasi/$1';

// rute keranjang
$route['keranjang/(:num)/(:any)'] = 'Keranjang/index/$1';
$route['keranjang/index/(:num)'] = 'Keranjang/index/$1';
$route['keranjang/saya'] = 'Keranjang/saya';
$route['keranjang/checkout'] = 'Keranjang/checkout';
$route['keranjang/buat_pesanan'] = 'Keranjang/buat_pesanan';

// rute ajax
$route['ajax/cari_produk_by_penjual'] = 'Ajax/cari_produk_by_penjual';
$route['ajax/ambil_produk_lain'] = 'Ajax/ambil_produk_lain';
$route['ajax/autocomplete'] = 'Ajax/autocomplete';
$route['ajax/ambil_kategori'] = 'Ajax/ambil_kategori';
$route['ajax/kirim_diskusi'] = 'Ajax/kirim_diskusi';
$route['ajax/kirim_balasan_diskusi'] = 'Ajax/kirim_balasan_diskusi';
$route['ajax/tawar_produk'] = 'Ajax/tawar_produk';
$route['ajax/ambil_kecamatan'] = 'Ajax/ambil_kecamatan';
$route['ajax/ambil_desa'] = 'Ajax/ambil_desa';
$route['ajax/ambil_ongkir'] = 'Ajax/ambil_ongkir';
$route['ajax/ambil_subkategori'] = 'Ajax/ambil_subkategori';
$route['ajax/ambil_pasar'] = 'Ajax/ambil_pasar';
$route['ajax/ambil_konten'] = 'Ajax/ambil_konten';
$route['ajax/cek_kelurahan'] = 'Ajax/cek_kelurahan';
$route['ajax/cek_keranjang'] = 'Ajax/cek_keranjang';
$route['ajax/notifikasi'] = 'Ajax/notifikasi';
$route['ajax/tutup_iklan'] = 'Ajax/tutup_iklan';
$route['ajax/tolak_pesanan'] = 'Ajax/tolak_pesanan';
$route['ajax/cek_promo'] = 'Ajax/cek_promo';
$route['keranjang/gantiJumlah'] = 'keranjang/gantiJumlah';
$route['keranjang/hapus'] = 'keranjang/hapus';

// Blog
$route['blog'] = 'blog/index';
$route['blog/(:any)'] = 'blog/baca/$1';

// Promo
$route['promo'] = 'promo/index';
$route['blog/(:any)'] = 'blog/baca/$1';

// rute manajemen profil
$route['profil'] = 'Profil/index';
$route['profil/edit'] = 'Profil/edit';
$route['profil/ganti_katasandi'] = 'Profil/ganti_katasandi';
$route['profil/pembelian'] = 'Profil/pembelian';
$route['profil/daftar_penjual'] = 'Profil/daftar_penjual';


$route['toko/saya'] = 'Toko/saya';
$route['toko/edit'] = 'Toko/edit';
// rute manajemen produk admin
$route['toko/produk'] = 'Toko/produk';
$route['toko/penjualan'] = 'Toko/penjualan';
$route['toko/produk/(:num)/(:any)'] = 'Toko/produk/$1';
$route['toko/produk/tambah_stok'] = 'Toko/tambah_stok';
$route['toko/produk/tambah'] = 'Toko/produk_tambah';
$route['toko/produk/ubah/(:num)/(:any)'] = 'Toko/produk_ubah/$1';
$route['toko/produk/status/(:num)'] = 'Toko/produk_status/$1';

// rute manajemen zonasi cod admin
$route['toko/cod'] = 'Toko/cod';
$route['toko/cod/tambah'] = 'Toko/cod_tambah';
$route['toko/cod/ubah/(:num)'] = 'Toko/cod_ubah/$1';
$route['toko/cod/hapus/(:num)'] = 'Toko/cod_hapus/$1';

// rute manajemen penjualan
$route['penjualan'] = 'Toko/penjualan';
$route['proses_penjualan/(:any)'] = 'Toko/proses_pesanan/$1';
$route['penjualan/(:any)'] = 'Toko/penjualan/$1';
$route['penjualan/(:any)/(:num)'] = 'Toko/penjualan/$1/$2';
$route['penjualan/detail/(:any)'] = 'Toko/penjualan_detail/$1';

// rute manajemen pembelian
$route['pembelian'] = 'Profil/pembelian';
$route['pembelian/(:any)'] = 'Profil/pembelian/$1';
$route['pembelian/(:any)/(:num)'] = 'Profil/pembelian/$1/$2';
$route['pembelian/detail/(:any)'] = 'Profil/pembelian_detail/$1';

$route['point'] = 'Profil/point/';


// rute page content
$route['about'] = 'Content/index/about';
$route['term-condition'] = 'Content/index/term-condition';
$route['cara-beli'] = 'Content/index/cara-beli';
$route['cara-jual'] = 'Content/index/cara-jual';
$route['faqs'] = 'Content/index/faqs';
$route['kebijakan-privasi'] = 'Content/index/kebijakan-privasi';

// rute produk
$route['produk'] = 'Frontend/produk';
$route['produk/(:num)'] = 'Frontend/produk';
$route['kirim_diskusi'] = 'Frontend/kirim_diskusi';
$route['frontend/produk'] = 'Frontend/produk';

// rute notifikasi
$route['notifikasi'] = 'Notifikasi/index';
$route['notifikasi/baca/(:num)'] = 'Notifikasi/baca/$1';

// rute penjual
$route['penjual'] = 'Frontend/penjual';
$route['penjual/(:num)'] = 'Frontend/penjual';
$route['frontend/penjual'] = 'Frontend/penjual';

$route['(:any)'] = 'Frontend/detail_penjual/$1';
// $route['penjual/(:num)/(:any)'] = 'Frontend/detail_penjual/$1';

$route['(:any)/(:any)'] = 'Frontend/detail_produk/$1/$2';
// $route['produk/(:num)/(:any)'] = 'Frontend/detail_produk/$1';