<?php
class Users_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function login($email, $password)
	{
		// $query = $this->db->get_where('tb_user', array('username'=>$username, 'password'=>$password));
		$query = $this->db->select('id_konsumen, nama_lengkap, status_user, penjual')
			->where('email', $email)
			->where('password', $password)
			->get('users');
		return $query->row_array();
	}

	public function get_data_after_update($id)
	{
		$query = $this->db->select('id_konsumen, nama_lengkap, status_user')
			->where('id_konsumen', $id)
			->get('users');
		return $query->row_array();
	}

	// update data penjual
	public function get_data_penjual_after_update($id)
	{
		$query = $this->db->select('id_penjual, nama_penjual, status_penjual')
			->where('id_penjual', $id)
			->get('penjual');
		return $query->row_array();
	}
}
