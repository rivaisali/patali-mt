<?php
// print_r($penjual);
// print_r($pemilik);
?>
<div class="row-cols-1 mt-4 px-1">
    <div class="col mb-4 profil-penjual">
        <div class="row align-items-start">
            <div class="col-12 col-sm-12 col-md-4 col-lg-2 col-xl-2 text-center mb-3">
                <img src="<?= base_url("uploads/users/" . $penjual->foto); ?>" alt="" class="img-thumbnail rounded-circle">
            </div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-10 col-xl-10">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 mb-3">
                        <h3><?= $penjual->nama_penjual; ?></h3>
                        <address class="my-0">
                            <?= $penjual->alamat_lengkap; ?>
                        </address>
                        <b class="d-block mb-1">
                            <?= get_kelurahan($penjual->kelurahan) . ", " . get_kecamatan($penjual->kecamatan) . ", " . get_kota($penjual->kota_id); ?>
                        </b>
                        <h6>
                            <span class="badge badge-primary" style="font-size: inherit;">
                                <i class="fas fa-user fa-fw"></i> <?= $pemilik->nama_lengkap; ?>
                            </span>
                            <!--<span class="badge badge-danger" style="font-size: inherit;">-->
                            <!--    <i class="fas fa-phone fa-fw"></i> <?= hp($penjual->no_telpon); ?>-->
                            <!--</span>-->
                        </h6>
                        <small class="text-muted d-block">
                            Login terakhir: <?= tgl_full($pemilik->last_login); ?>
                        </small>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 justify-content-center">
                        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-2 text-center border-bottom border-primary pt-3 mb-3">
                            <div class="col text-primary">
                                <h3><i class="fas fa-store-alt"></i> <?= $penjual->buka; ?></h3>
                            </div>
                            <div class="col text-danger">
                                <h3><i class="fas fa-store-alt-slash"></i> <?= $penjual->tutup; ?></h3>
                            </div>
                        </div>



                        <div class="input-group mb-3">
                            <input type="text" class="form-control" id="keywordProduk" placeholder="Cari Produk" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary cariProdukByPenjual" type="button">Cari</button>
                                <button class="btn btn-danger resetCariProdukByPenjual" type="button">Reset</button>
                            </div>
                            <!-- <div class="input-group-append">
                                <button class="btn btn-primary " type="button" id="button-addon2">Cari</button>
                            </div> -->
                        </div>
                        <?php
                        // print_r($penjual);
                        ?>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="col produk-penjual" id="">
        <input type="hidden" name="idPenjual" id="idPenjual" value="<?= $penjual->id_penjual; ?>">
        <input type="hidden" name="penjualSeo" id="penjualSeo" value="<?= $penjual->penjual_seo; ?>">
        <input type="hidden" name="jumlahProduk" id="jumlahProduk" value="<?= $jumlah_produk; ?>">
        <input type="hidden" name="pageData" id="pageData" value="<?= $page_data; ?>">
        <div class="row row-cols-2 row-cols-sm-12 row-cols-md-4 row-cols-lg-6" id="container-produk">
            <?php foreach ($produk as $p) : ?>
                <div class="col mb-3 p-1" id="product">
                    <a href="<?= base_url($penjual->penjual_seo . '/' . $p->produk_seo); ?>" class="text-decoration-none">
                        <div class="card h-100 product rounded">
                            <div class="thumbnail">
                                <img src="<?= base_url('uploads/produk/' . $p->gambar); ?>" class="card-img-top rounded-top">
                            </div>
                            <div class="card-body">
                                <div class="card-text">
                                    <b class="text-truncate text-dark d-block"><?= $p->nama_produk; ?></b>
                                    <h6 class="text-primary font-weight-bolder"><?= rupiah($p->harga_konsumen); ?></h6>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="d-block text-center">
            <a href="#" class="btn btn-primary" id="loadProdukOther">
                Lihat Lebih Banyak.
            </a>
        </div>
    </div>
</div>