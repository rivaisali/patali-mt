<div class="container-fluid mt-4">
    <div class="row row-cols-2 px-2">
        <div class="col-12 col-md-3 col-lg-3 mb-5 h-50" id="filter">
            <div class="card shadow-lg rounded-lg mb-3">
                <div class="card-header bg-primary text-white">
                    Filter
                </div>
                <div class="card-body" id="box-filter">
                    <?= form_open(base_url("penjual"), ["autocomplete" => "off", "method" => "get"]); ?>
                    <input type="text" class="form-control form-control-sm" name="keyword" placeholder="Kata Kunci" value="<?= (cari_penjual("keyword")) ? cari_penjual("keyword") : ''; ?>">
                    <div class="btn-group btn-group-sm btn-block mt-3">
                        <input type="submit" value="Cari" name="cari_penjual" class="btn btn-primary">
                        <input type="submit" value="Hapus" name="hapus" class="btn btn-dark">
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>

            <ul class="nav nav-pills flex-column p-0">
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url("produk"); ?>">
                        <div class="fas fa-briefcase fa-fw"></div> Produk
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?= base_url("penjual"); ?>"><i class="fas fa-store"></i> Penjual</a>
                </li>
            </ul>
        </div>
        <div class="col-12 col-md-9 col-lg-9">
            <div class="row row-cols-1">
                <!-- Marketing Icons Section -->
                <div class="col">
                    <div class="row row-cols-1">
                        <div class="col mb-5">
                            <?php
                            if (empty($penjual)) {
                            ?>
                                <div class="alert alert-success" role="alert">
                                    <h4 class="alert-heading">Oooops..!</h4>
                                    <p>Tidak ada penjual untuk ditampilkan.</p>
                                </div>
                            <?php
                            } else {
                            ?>
                                <div class="container-fluid">
                                    <div class="row row-cols-2 row-cols-sm-2 row-cols-md-4 row-cols-lg-5" id="container-penjual">
                                        <?php foreach ($penjual as $p) : ?>
                                            <div class="col p-1 mb-3">
                                                <!-- <a href="<?= base_url('penjual/' . $p->id_penjual . '/' . $p->penjual_seo); ?>" class="text-decoration-none"> -->
                                                <a href="<?= base_url($p->penjual_seo); ?>" class="text-decoration-none">
                                                    <div class="card h-100 rounded penjual">
                                                        <div class="thumbnail">
                                                            <img src="<?= base_url('uploads/users/' . $p->foto); ?>" class="card-img-top rounded-top">
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="card-text">
                                                                <b class="text-dark d-block text-center"><?= $p->nama_penjual; ?></b>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col mb-3">
                            <!--Tampilkan pagination-->
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>