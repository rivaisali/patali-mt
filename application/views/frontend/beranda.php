<div class="row-cols-sm-1 overflow-hidden my-3">
    <div class="">
        <section class="baner slider">
            <?php //if (empty($banner)): ?>
            <div class="col">
                <img src="<?=base_url('uploads/baner/6.jpg');?>" class="rounded-lg">
            </div>
            <div class="col">
                <img src="<?=base_url('uploads/baner/6.jpg');?>" class="rounded-lg">
            </div>
            <div class="col">
                <img src="<?=base_url('uploads/baner/6.jpg');?>" class="rounded-lg">
            </div>
            <div class="col">
                <img src="<?=base_url('uploads/baner/6.jpg');?>" class="rounded-lg">
            </div>
            <div class="col">
                <img src="<?=base_url('uploads/baner/6.jpg');?>" class="rounded-lg">
            </div>
            <?php
//else:
// foreach ($banner as $ban):
?>
		            <!-- <div class="col">
		                <img src="<?php //"https://be.sipardi.id/" . //$ban->gambar;?>" class="rounded-lg">
		            </div> -->

		            <?php
?>
        </section>
    </div>
</div>



<div class="row-cols-1" id="produk-beranda">

    <!-- Toko Populer -->
    <div class="col card rounded">
        <div class="row row-cols-1">
            <div class="col mb-100" style="margin-top:10px;">
                <h4 class="d-block text-dark font-weight-bold p-2">
                    Toko Populer
                    <small class="float-right">
                        <a class="btn btn-link text-danger text-decoration-none" href="<?=base_url("penjual");?>">Lihat
                            Semua</a>
                    </small>
                </h4>
            </div>
            <div class="col mb-3">
                <div class="container-fluid">
                    <div class="row row-cols-2 row-cols-sm-2 row-cols-md-2 row-cols-md-6 container-beranda">
                        <?php foreach ($toko_populer as $p): ?>
                        <div class="col mb-3 p-1" data-toggle="tooltip" data-placement="top"
                            title="<?=$p->nama_penjual;?>">
                            <a href="<?=base_url($p->penjual_seo);?>" class="text-decoration-none">
                                <div class="card rounded-sm">
                                    <div class="thumbnail p-5">
                                        <img class="w-100" src="<?=base_url('uploads/users/' . $p->foto);?>"
                                            class="card-img-top rounded-top" alt="avatar">
                                    </div>

                                </div>
                            </a>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <br>
    <!-- Product Populer -->
    <?php if ($produk_populer): ?>
    <div class="col mb-3 card rounded">
        <div class="row row-cols-1">
            <div class="col mb-100" style="margin-top:10px;">
                <h4 class="d-block text-dark font-weight-bold p-2">
                    Produk Populer
                    <small class="float-right">
                        <a class="btn btn-link text-danger text-decoration-none" href="<?=base_url("produk");?>">Lihat
                            Semua</a>
                    </small>
                </h4>
            </div>
            <div class="col mb-3">
                <div class="container-fluid w-100">
                    <div class="row">
                        <div class="row row-cols-2 row-cols-sm-2 row-cols-md-4 row-cols-lg-6 container-beranda">
                            <?php foreach ($produk_populer as $p):
?>
                            <div class="col mb-3 p-1" id="product" data-toggle="tooltip" data-placement="top"
                                title="<?=$p->nama_produk;?>">
                                <!-- <a href="<?=base_url('produk/' . $p->id_produk . '/' . $p->produk_seo);?>" class="text-decoration-none"> -->
                                <a href="<?=base_url($p->penjual_seo . '/' . $p->produk_seo);?>"
                                    class="text-decoration-none">
                                    <div class="card h-100 product rounded p-1">
                                        <?php
$cek_operasional = cek_operasional($p->id_penjual);
if ($cek_operasional) {
    echo "<div class='ribbon ribbon-top-left'><span><i class='fas fa-store-slash'></i> tutup</span></div>";
}
?>



                                        <img class="card-img-top" height="150px"
                                            src="<?=base_url('uploads/produk/' . $p->gambar);?>" alt="Card image cap">
                                        <!-- <div class="thumbnail">
                                        <img src="<?=base_url('uploads/produk/' . $p->gambar);?>"
                                            class="card-img-top rounded-top img-responsive" height="">
                                    </div> -->
                                        <div class="card-body">
                                            <div class="card-text">
                                                <small class="text-truncate text-danger d-block"><i
                                                        class="fa fa-store text-danger"></i>&nbsp;<?=$p->nama_penjual;?></small>
                                                <small
                                                    class="text-truncate text-dark d-block"><?=$p->nama_produk;?></small>
                                                <?php
if ($p->diskon > 0):
    $new_harga = $p->harga_konsumen - (($p->diskon / 100) * $p->harga_konsumen);
    ?>
						                                                <s class="text-muted small"><?=rupiah($p->harga_konsumen);?></s>
						                                                <small
						                                                    class="text-primary font-weight-bolder"><?=rupiah($new_harga);?></small>
						                                                <?php if ($p->diskon > 0): ?>
						                                                &nbsp;<span
						                                                    class="badge badge-warning text-white"><?=$p->diskon;?>%</span>

						                                                <?php endif;?>

                                                <?php else: ?>
                                                <small
                                                    class="text-primary font-weight-bolder"><?=rupiah($p->harga_konsumen);?>
                                                </small>
                                                <?php endif;?>
                                            </div>

                                        </div>
                                        <button class="btn btn-primary" style="font-size:12px;">Beli</button>

                                    </div>

                                </a>
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php endif;?>


     <!-- Product Populer -->
    <?php if ($produk_baru): ?>
    <div class="col mb-3 card rounded">
        <div class="row row-cols-1">
            <div class="col mb-100" style="margin-top:10px;">
                <h4 class="d-block text-dark font-weight-bold p-2">
                    Produk Terbaru
                    <small class="float-right">
                        <a class="btn btn-link text-danger text-decoration-none" href="<?=base_url("produk");?>">Lihat
                            Semua</a>
                    </small>
                </h4>
            </div>
            <div class="col mb-3">
                <div class="container-fluid w-100">
                    <div class="row">
                        <div class="row row-cols-2 row-cols-sm-2 row-cols-md-4 row-cols-lg-6 container-beranda">
                            <?php foreach ($produk_baru as $p):
?>
                            <div class="col mb-3 p-1" id="product" data-toggle="tooltip" data-placement="top"
                                title="<?=$p->nama_produk;?>">
                                <!-- <a href="<?=base_url('produk/' . $p->id_produk . '/' . $p->produk_seo);?>" class="text-decoration-none"> -->
                                <a href="<?=base_url($p->penjual_seo . '/' . $p->produk_seo);?>"
                                    class="text-decoration-none">
                                    <div class="card h-100 product rounded p-1">
                                        <?php
$cek_operasional = cek_operasional($p->id_penjual);
if ($cek_operasional) {
    echo "<div class='ribbon ribbon-top-left'><span><i class='fas fa-store-slash'></i> tutup</span></div>";
}
?>



                                        <img class="card-img-top" height="150px"
                                            src="<?=base_url('uploads/produk/' . $p->gambar);?>" alt="Card image cap">
                                        <!-- <div class="thumbnail">
                                        <img src="<?=base_url('uploads/produk/' . $p->gambar);?>"
                                            class="card-img-top rounded-top img-responsive" height="">
                                    </div> -->
                                        <div class="card-body">
                                            <div class="card-text">
                                                <small class="text-truncate text-danger d-block"><i
                                                        class="fa fa-store text-danger"></i>&nbsp;<?=$p->nama_penjual;?></small>
                                                <small
                                                    class="text-truncate text-dark d-block"><?=$p->nama_produk;?></small>
                                                <?php
if ($p->diskon > 0):
    $new_harga = $p->harga_konsumen - (($p->diskon / 100) * $p->harga_konsumen);
    ?>
						                                                <s class="text-muted small"><?=rupiah($p->harga_konsumen);?></s>
						                                                <small
						                                                    class="text-primary font-weight-bolder"><?=rupiah($new_harga);?></small>
						                                                <?php if ($p->diskon > 0): ?>
						                                                &nbsp;<span
						                                                    class="badge badge-warning text-white"><?=$p->diskon;?>%</span>

						                                                <?php endif;?>

                                                <?php else: ?>
                                                <small
                                                    class="text-primary font-weight-bolder"><?=rupiah($p->harga_konsumen);?>
                                                </small>
                                                <?php endif;?>
                                            </div>

                                        </div>
                                        <button class="btn btn-primary" style="font-size:12px;">Beli</button>

                                    </div>

                                </a>
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php endif;?>


   <!-- Toko Lainnya -->
    <div class="col card rounded">
        <div class="row row-cols-1">
            <div class="col mb-100" style="margin-top:10px;">
                <h4 class="d-block text-dark font-weight-bold p-2">
                    Toko Lainnya
                    <small class="float-right">
                        <a class="btn btn-link text-danger text-decoration-none" href="<?=base_url("penjual");?>">Lihat
                            Semua</a>
                    </small>
                </h4>
            </div>
            <div class="col mb-3">
                <div class="container-fluid">
                    <div class="row row-cols-2 row-cols-sm-2 row-cols-md-2 row-cols-md-6 container-beranda">
                        <?php foreach ($toko_lainnya as $p): ?>
                        <div class="col mb-3 p-1" data-toggle="tooltip" data-placement="top"
                            title="<?=$p->nama_penjual;?>">
                            <a href="<?=base_url($p->penjual_seo);?>" class="text-decoration-none">
                                <div class="card rounded-sm">
                                    <div class="thumbnail p-5">
                                        <img class="w-100" src="<?=base_url('uploads/users/' . $p->foto);?>"
                                            class="card-img-top rounded-top" alt="avatar">
                                    </div>

                                </div>
                            </a>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>

        </div>
    </div>





</div>