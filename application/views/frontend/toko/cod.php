<div class="row p-2">
    <div class="col-md-12">
        <a href="<?= base_url("toko/cod/tambah"); ?>" class="btn btn-outline-primary mb-3">Tambah Lokasi</a>
        <table class="table table-hover nowrap" id="datatable">
            <thead>
                <tr>
                    <th data-priority="1">No.</th>
                    <th>Kab / Kota</th>
                    <th>Kecamatan</th>
                    <th data-priority="2">Kelurahan</th>
                    <th>Harga</th>
                    <th>Aksi</th>
                </tr>
            <tbody>
                <?php
                $no = 1;
                foreach ($data as $d) :
                ?>
                    <tr>
                        <th><?= $no++; ?></th>
                        <td><?= get_kota($d->kabupaten_id); ?></td>
                        <td><?= get_kecamatan($d->kecamatan_id); ?></td>
                        <td><?= get_kelurahan($d->desa_id); ?></td>
                        <td><?= rupiah($d->biaya_cod); ?></td>
                        <td width="20">
                            <div class="btn-group btn-group-sm">
                                <a href="<?= base_url("toko/cod/ubah/" . $d->id_cod); ?>" class="btn btn-primary">Ubah</a>
                                <!-- <a href="<?= base_url("toko/cod/hapus/" . $d->id_cod); ?>" class="btn btn-danger">Hapus</a> -->
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>