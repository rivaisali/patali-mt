<div class="row p-2">
    <div class="col-md-12">
        <a href="<?= base_url("toko/produk/tambah"); ?>" class="btn btn-outline-primary mb-3">Tambah Produk</a>
        <div class="float-right">
            <ul class="list-inline">
                <li class="list-inline-item"><i class="fas fa-circle text-success"></i> Aktif</li>
                <li class="list-inline-item"><i class="fas fa-circle text-danger"></i> Tidak Aktif</li>
            </ul>
        </div>
        <table class="table table-hover nowrap" id="datatable">
            <thead>
                <tr>
                    <th data-priority="1">No.</th>
                    <th width="10">#</th>
                    <th data-priority="2">Nama Produk</th>
                    <th>Harga</th>
                    <th>Hits</th>
                    <th>Aksi</th>
                </tr>
            <tbody>
                <?php
                $no = 1;
                foreach ($data as $d) :
                    if ($d->status_produk == "1") {
                        $warna = "success";
                        $text = "Nonaktifkan";
                    } else {
                        $warna = "danger";
                        $text = "Aktifkan";
                    }
                ?>
                    <tr>
                        <th>
                            <?= $no++; ?>
                        </th>
                        <td>
                            <i class="fas fa-circle text-<?= $warna; ?>"></i>
                        </td>
                        <td><?= $d->nama_produk; ?></td>
                        <td><?= $d->harga_konsumen; ?></td>
                        <td><?= $d->hits; ?></td>
                        <td width="20">
                            <div class="btn-group btn-group-sm">
                                <a href="<?= base_url("toko/produk/" . $d->id_produk . "/" . $d->produk_seo); ?>" class="btn btn-dark">Detail</a>
                                <a href="<?= base_url("toko/produk/ubah/" . $d->id_produk . "/" . $d->produk_seo); ?>" class="btn btn-primary">Ubah</a>
                                <a href="<?= base_url("toko/produk/status/" . $d->id_produk); ?>" class="btn btn-secondary"><?= $text; ?></a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>