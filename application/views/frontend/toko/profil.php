<div class="row p-2">
    <div class="col-md-4 col-12 mb-3">
        <img src="<?= base_url('uploads/users/' . $profil->foto); ?>" class="img-thumbnail rounded-lg">
        <a href="<?= base_url('toko/edit'); ?>" class="btn btn-outline-primary btn-block mt-2"><i class="fas fa-edit fa-fw"></i> Edit Profil Toko</a>
    </div>
    <div class="col-md-8 col-12">
        <div class="row  mb-4">
            <div class="col-sm-4 col-6">Nama</div>
            <div class="col-sm-8 col-6 font-weight-bold"><?= $profil->nama_penjual; ?></div>
        </div>
        <div class="row  mb-4">
            <div class="col-sm-4 col-6">No Handphone</div>
            <div class="col-sm-8 col-6 font-weight-bold"><?= $profil->no_telpon; ?></div>
        </div>
        <div class="row  mb-4">
            <div class="col-sm-4 col-6">Alamat</div>
            <div class="col-sm-8 col-6 font-weight-bold"><?= $profil->alamat_lengkap; ?></div>
        </div>
        <div class="row  mb-4">
            <div class="col-sm-4 col-6">Kel / Desa</div>
            <div class="col-sm-8 col-6 font-weight-bold"><?= get_kelurahan($profil->kelurahan); ?></div>
        </div>
        <div class="row  mb-4">
            <div class="col-sm-4 col-6">Kecamatan</div>
            <div class="col-sm-8 col-6 font-weight-bold"><?= get_kecamatan($profil->kecamatan); ?></div>
        </div>
        <div class="row  mb-4">
            <div class="col-sm-4 col-6">Kab / Kota</div>
            <div class="col-sm-8 col-6 font-weight-bold"><?= get_kota($profil->kota_id); ?></div>
        </div>
        <div class="row  mb-4">
            <div class="col-sm-4 col-6">Waktu Operasional</div>
            <div class="col-sm-8 col-6 font-weight-bold">
                <?= substr($profil->buka, 0, 5) . " s/d " . substr($profil->tutup, 0, 5); ?>
            </div>
        </div>
    </div>
</div>