<div class="row p-2">
    <div class="col-md-12">
        <h5>
            <?= $method; ?> Zonasi COD
            <a href="<?= base_url('toko/cod'); ?>" class="btn btn-dark float-right btn-sm">Kembali</a>
        </h5>
        <hr>
        <?= form_open("", ["autocomplete" => "off", "id" => "cod"]); ?>
        <input type="hidden" name="method" value="<?= strtolower($method); ?>">
        <?php if (!empty($data)) {
            echo '<input type="hidden" name="id_cod" value="' . $data->id_cod . '">';
        } ?>
        <div class="form-group row mb-4">
            <label for="" class="col-sm-2 col-form-label">
                Pilih Lokasi
            </label>
            <div class="col-md-10">
                <div class="form-row">
                    <div class="col-12 col-sm-4 mb-2">
                        <select class="form-control <?= (form_error('kota_id')) ? 'is-invalid' : ''; ?>" id="kabkota" name="kota_id" autofocus>
                            <option selected disabled>Pilih Kab / Kota</option>
                            <?php foreach ($kota as $k) { ?>
                                <option value="<?= $k->kota_id; ?>" <?= (!empty($data) && $k->kota_id == $data->kabupaten_id) ? 'selected' : ''; ?> <?= set_select('kota_id', (!empty($data)) ? $data->kabupaten_id : '', FALSE); ?>>
                                    <?= $k->nama_kota; ?>
                                </option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('kota_id'); ?>
                    </div>
                    <div class="col-12 col-sm-4 mb-2">
                        <select class="form-control <?= (form_error('kecamatan')) ? 'is-invalid' : ''; ?>" id="kecamatan" name="kecamatan">
                            <option selected disabled>Pilih Kecamatan</option>
                            <?php if (!empty($data) && $data->kecamatan_id != "0") { ?>
                                <?php
                                $kecamatan = ambil_data_by_id('rb_kecamatan', 'kota_id', $data->kabupaten_id);
                                foreach ($kecamatan as $kec) :
                                ?>
                                    <option value="<?= $kec->kecamatan_id; ?>" <?= ($kec->kecamatan_id == $data->kecamatan_id) ? 'selected' : ''; ?>>
                                        <?= $kec->nama_kecamatan ?>
                                    </option>
                                <?php
                                endforeach;
                                ?>
                                <!--<option value="<?= $data->kecamatan_id; ?>" selected>-->
                                <!--    <?= ambil_nama_by_id("rb_kecamatan", "nama_kecamatan", "kecamatan_id", $data->kecamatan_id); ?>-->
                                <!--</option>-->
                            <?php } ?>
                        </select>
                        <?php echo form_error('kecamatan'); ?>
                    </div>
                    <div class="col-12 col-sm-4 mb-2">
                        <select class="form-control <?= (form_error('kelurahan')) ? 'is-invalid' : ''; ?>" id="kelurahan" name="kelurahan">
                            <option selected disabled>Pilih Kelurahan</option>
                            <?php if (!empty($data) && $data->desa_id != "0") { ?>
                                <?php
                                $kelurahan = ambil_data_by_id('rb_desa', 'kecamatan_id', $data->kecamatan_id);
                                foreach ($kelurahan as $kel) :
                                ?>
                                    <option value="<?= $kel->desa_id; ?>" <?= ($kel->desa_id == $data->desa_id) ? 'selected' : ''; ?>>
                                        <?= $kel->nama_desa ?>
                                    </option>
                                <?php
                                endforeach;
                                ?>
                                <!--<option value="<?= $data->desa_id; ?>" selected>-->
                                <!--    <?= ambil_nama_by_id("rb_desa", "nama_desa", "desa_id", $data->desa_id); ?>-->
                                <!--</option>-->
                            <?php } ?>
                        </select>
                        <?php echo form_error('kelurahan'); ?>

                        <div class="info-kelurahan"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="harga" class="col-sm-2 col-form-label">Harga</label>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-md-4">
                        <input type="text" class="form-control <?= (form_error('harga')) ? 'is-invalid' : ''; ?>" id="harga" name="harga" onkeypress="hanyaAngka()" placeholder="Masukkan hanya angka." value="<?= set_value("nama_produk", ($method == "Ubah") ? $data->biaya_cod : ''); ?>">
                        <?php echo form_error('harga'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary"><?= $method; ?></button>
                <button type="reset" class="btn btn-secondary">Batal</button>
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>