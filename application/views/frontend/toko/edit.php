<div class="row p-2">
    <div class="col-md-12">
        <?php echo form_open('', 'method="POST" autocomplete="off" enctype="multipart/form-data"');
        ?>
        <div class="form-group row mb-4">
            <label for="nama_penjual" class="col-sm-2 col-form-label">Nama Toko / Usaha</label>
            <div class="col-sm-10">
                <input type="text" class="form-control <?= (form_error('nama_penjual')) ? 'is-invalid' : ''; ?>" id="nama_penjual" name="nama_penjual" value="<?= set_value("nama_penjual", $data->nama_penjual, false) ?>" autofocus>
                <?php echo form_error('nama_penjual'); ?>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="no_hp" class="col-sm-2 col-form-label">No. HP</label>
            <div class="col-sm-10">
                <input type="text" class="form-control <?= (form_error('no_telpon')) ? 'is-invalid' : ''; ?>" id="no_telpon" name="no_telpon" value="<?= set_value("no_telpon", $data->no_telpon, false) ?>">
                <?php echo form_error('no_telpon'); ?>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="alamat_lengkap" class="col-sm-2 col-form-label">Alamat Lengkap</label>
            <div class="col-sm-10">
                <textarea name="alamat_lengkap" id="alamat_lengkap" rows="2" class="form-control <?= (form_error('alamat_lengkap')) ? 'is-invalid' : ''; ?>"><?= set_value("alamat_lengkap", $data->alamat_lengkap, false); ?></textarea>
                <?php echo form_error('alamat_lengkap'); ?>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="" class="col-sm-2 col-form-label"></label>
            <div class="col-md-10">
                <div class="form-row">
                    <div class="col">
                        <select class="form-control <?= (form_error('kota_id')) ? 'is-invalid' : ''; ?>" id="kabkota" name="kota_id">
                            <option selected disabled>Pilih Kab / Kota</option>
                            <?php foreach ($kota as $k) { ?>
                                <option value="<?= $k->kota_id; ?>" <?= ($k->kota_id == $data->kota_id) ? 'selected' : ''; ?> <?= set_select('kota_id', $k->kota_id, FALSE); ?>>
                                    <?= $k->nama_kota; ?>
                                </option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('kota_id'); ?>
                    </div>
                    <div class="col">
                        <select class="form-control <?= (form_error('kecamatan')) ? 'is-invalid' : ''; ?>" id="kecamatan" name="kecamatan">
                            <option selected disabled>Pilih Kecamatan</option>
                            <?php if ($data->kecamatan != "0") { ?>
                                <option value="<?= $data->kecamatan; ?>" selected>
                                    <?= ambil_nama_by_id("rb_kecamatan", "nama_kecamatan", "kecamatan_id", $data->kecamatan); ?>
                                </option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('kecamatan'); ?>
                    </div>
                    <div class="col">
                        <select class="form-control <?= (form_error('kelurahan')) ? 'is-invalid' : ''; ?>" id="kelurahan" name="kelurahan">
                            <option selected disabled>Pilih Kelurahan</option>
                            <?php if ($data->kelurahan != "0") { ?>
                                <option value="<?= $data->kelurahan; ?>" selected>
                                    <?= ambil_nama_by_id("rb_desa", "nama_desa", "desa_id", $data->kelurahan); ?>
                                </option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('kelurahan'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="waktu_buka" class="col-sm-2 col-form-label">Waktu Operasional</label>
            <div class="col-sm-10">
                <div class="form-row">
                    <div class="col">
                        <input title="Waktu Buka" type="text" class="form-control" name="buka" id="timepicker1" placeholder="Waktu Buka" value="<?= set_value('buka', substr($data->buka, 0, 5)); ?>">
                    </div>
                    <div class="col">
                        <input title="Waktu Tutup" type="text" class="form-control" name="tutup" id="timepicker2" placeholder="Waktu Tutup" value="<?= set_value('tutup', substr($data->tutup, 0, 5)); ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="alamat_lengkap" class="col-sm-2 col-form-label">Foto</label>
            <div class="col-sm-2">
                <img src="<?= base_url("uploads/users/" . $data->foto); ?>" class="img-thumbnail img-preview">
            </div>
            <div class="col-sm-8">
                <div class="custom-file">
                    <input type="hidden" name="foto_lama" value="<?= $data->foto; ?>">
                    <input type="file" class="custom-file-input form-control <?= (form_error('gambar')) ? 'is-invalid' : ''; ?>" id="foto" name="gambar" onchange="previewImg()" aria-describedby="fotoHelpBlock">
                    <label class="custom-file-label" for="foto">
                        <?= ($data->foto == 'default.jpg' || $data->foto == "") ? 'Pilih Gambar' : $data->foto; ?>
                    </label>
                    <small id="fotoHelpBlock" class="form-text text-muted">
                        Kosongkan jika tidak ingin mengganti foto.
                    </small>
                    <?php echo form_error('gambar'); ?>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10 col-12">
                <button type="submit" name="simpan" class="btn btn-primary" value="simpan">Simpan</button>
                <button type="reset" class="btn btn-danger">Reset</button>
                <a href="<?= base_url('profil'); ?>" class="btn btn-dark">Kembali</a>
            </div>
        </div>
        <?= form_close(); ?>
        <!-- </form> -->
    </div>
</div>