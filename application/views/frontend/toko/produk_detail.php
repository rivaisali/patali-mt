<div class="row p-3">
    <div class="col-12 mb-3 text-right">
        <a href="<?= base_url("toko/produk"); ?>" class="btn btn-dark d-block d-md-inline-block">Kembali</a>
    </div>
    <div class="col-md-4 col-12 text-center">
        <img src="<?= base_url("uploads/produk/" . $data->gambar); ?>" class="img-fluid img-thumbnail shadow mb-3">
        <ul class="list-group-flush d-block p-0 bg-light">
            <li class="list-group-item bg-transparent">Diunggah: <?= tgl_indonesia($data->waktu_input); ?></li>
            <li class="list-group-item bg-transparent">Dilihat: <?= $data->hits . "x"; ?></li>
        </ul>
    </div>
    <div class="col-md-4 col-12">
        <ul class="list-group-flush d-block p-0">
            <li class="list-group-item bg-transparent">
                <h5 class="font-weight-bold"><?= $data->nama_produk; ?></h5>
            </li>
            <li class="list-group-item bg-transparent">Kategori : <b><?= ucwords(strtolower(get_kategori($data->id_kategori_produk))); ?></b></li>
            <li class="list-group-item bg-transparent">Harga : <b><?= rupiah($data->harga_konsumen); ?></b></li>
            <li class="list-group-item bg-transparent">Diskon : <b><?= $data->diskon; ?></b></li>
            <li class="list-group-item bg-transparent">Berat : <b><?= $data->berat . "gram"; ?></b></li>
            <li class="list-group-item bg-transparent">Kondisi : <b><?= $data->kondisi; ?></b></li>
        </ul>
    </div>
    <div class="col-md-4 col-12">
        <?= $data->deskripsi; ?>
    </div>
    <hr class="d-block w-100">

    <div class="col-md-12 border border-white shadow p-3 bg-white">
        <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="stok-tab" data-toggle="tab" href="#stok" role="tab" aria-controls="stok" aria-selected="true">Stok</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="diskusi-tab" data-toggle="tab" href="#diskusi" role="tab" aria-controls="diskusi" aria-selected="false">Diskusi</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="stok" role="tabpanel" aria-labelledby="stok-tab">
                <div class="row">
                    <div class="col-md-6 col-12 mb-4">
                        <table class="table table-bordered nowrap" id="datatable">
                            <thead>
                                <tr>
                                    <th data-priority="1">Tgl</th>
                                    <th data-priority="2">Stok Masuk</th>
                                    <th>Keterangan</th>
                                    <!-- <th>#</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($stok as $s) : ?>
                                    <tr>
                                        <td><?= tgl_laporan($s->waktu_input); ?></td>
                                        <td><?= $s->stok; ?></td>
                                        <td><?= $s->keterangan; ?></td>
                                        <!-- <td>
                                            <?php if ($s->keterangan != "Stok Awal") { ?>
                                                <a href="<?= base_url("toko/hapus_stok/" . $s->id_stok); ?>" class="btn btn-danger btn-sm">
                                                    <i class="fas fa-trash-alt fa-fw fa-sm"></i>
                                                </a>
                                            <?php } ?>
                                        </td> -->
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="card">
                            <div class="card-header"><b class="card-title">Tambah Stok</b></div>
                            <div class="card-body">
                                <?= form_open("toko/produk/tambah_stok", ["autocomplete" => "off"]); ?>
                                <?= form_hidden("id_produk", $data->id_produk); ?>
                                <?= form_hidden("id_penjual", penjual("id_penjual")); ?>
                                <div class="form-group row">
                                    <label for="stok" class="col-sm-3 col-form-label">Stok</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control <?= (form_error('stok')) ? 'is-invalid' : ''; ?>" id="stok" name="stok" placeholder="Masukkan Hanya Angka" value="<?= set_value("stok", ''); ?>" onkeypress="hanyaAngka()" autofocus>
                                        <?php echo form_error('stok'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="keterangan" class="col-sm-3 col-form-label">Keterangan</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control <?= (form_error('keterangan')) ? 'is-invalid' : ''; ?>" name="keterangan" id="keterangan" rows="2" placeholder="Keterangan tentang penambahan stok."></textarea>
                                        <?php echo form_error('keterangan'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Tambah</button>
                                        <button type="reset" class="btn btn-secondary">Batal</button>
                                    </div>
                                </div>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="diskusi" role="tabpanel" aria-labelledby="diskusi-tab">
                <div class="row">
                    <div class="col-12 mb-4 table-responsive">
                        <table class="table table-bordered" id="datatable2" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th data-priority="1">Tgl</th>
                                    <th data-priority="2">Pesan</th>
                                    <th>Pengirim</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($diskusi as $d) : ?>
                                    <tr>
                                        <td><?= tgl_laporan($d->create_at); ?></td>
                                        <td class="text-muted">
                                            <small><?= $d->pesan; ?></small>
                                            <?php if ($d->status == "1") {
                                                echo "<br>Jawaban anda: <b>" . ambil_nama_by_id("diskusi", "pesan", "reply_for", $d->id_diskusi) . "</b>";
                                            } ?>
                                        </td>
                                        <td><?= ambil_nama_by_id("users", "nama_lengkap", "id_konsumen", $d->pengirim); ?></td>
                                        <td>
                                            <?php if ($d->status == "0") { ?>
                                                <a title="Balas" href="#" class="btn btn-primary btn-sm balasDiskusi" id="balasDiskusi" data-pesan="<?= $d->pesan; ?>" data-idproduk="<?= $d->id_produk; ?>" data-iddiskusi="<?= $d->id_diskusi; ?>">
                                                    <i class="fas fa-reply fa-fw fa-sm"></i>
                                                </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>