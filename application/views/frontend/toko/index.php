<div class="container mt-3">
    <div class="row">
        <div class="col-12 mb-2 p-2">
            <nav class="navbar-expand-lg">
                <button class="navbar-toggler btn-block py-3 bg-primary text-white mb-2" type="button" data-toggle="collapse" data-target="#menuProfil" aria-controls="menuProfil" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fas fa-bars"></span> Menu Toko
                </button>
                <div class="collapse navbar-collapse" id="menuProfil">
                    <ul class="list-group list-group-horizontal w-100 text-center">
                        <!--<a title="Profil Toko" href="<?= base_url("toko/saya"); ?>" class="list-group-item list-group-item-action <?= ($subpage == "profil" || $subpage == "edit") ? 'active' : ''; ?>">-->
                        <!--    <i class="fas fa-store fa-fw"></i> <span class="d-none d-sm-block">Profil Toko</span>-->
                        <!--</a>-->
                        <!--<a title="Produk" href="<?= base_url("toko/produk"); ?>" class="list-group-item list-group-item-action <?= ($subpage == "produk" || $subpage == "produk_detail" || $subpage == "produk_aksi") ? 'active' : ''; ?>">-->
                        <!--    <i class="fas fa-briefcase fa-fw"></i> <span class="d-none d-sm-block">Produk</span>-->
                        <!--</a>-->
                        <!--<a title="Penjual" href="<?= base_url("penjualan"); ?>" class="list-group-item list-group-item-action <?= ($subpage == "penjualan" || $subpage == "penjualan_detail") ? 'active' : ''; ?>">-->
                        <!--    <i class="fas fa-cart-arrow-down fa-fw"></i> <span class="d-none d-sm-block">Penjualan</span>-->
                        <!--</a>-->
                        <!--<a title="Zonasi COD" href="<?= base_url("toko/cod"); ?>" class="list-group-item list-group-item-action <?= ($subpage == "cod" || $subpage == "cod_aksi") ? 'active' : ''; ?>">-->
                        <!--    <i class="fas fa-truck-moving fa-fw"></i> <span class="d-none d-sm-block">Zonasi COD</span>-->
                        <!--</a>-->
                        <a title="Profil Toko" href="<?= base_url("toko/saya"); ?>" class="list-group-item list-group-item-action <?= ($subpage == "profil" || $subpage == "edit") ? 'active' : ''; ?>">
                            <i class="fas fa-store fa-fw"></i> <span class="d-block small">Profil Toko</span>
                        </a>
                        <a title="Produk" href="<?= base_url("toko/produk"); ?>" class="list-group-item list-group-item-action <?= ($subpage == "produk" || $subpage == "produk_detail" || $subpage == "produk_aksi") ? 'active' : ''; ?>">
                            <i class="fas fa-briefcase fa-fw"></i> <span class="d-block small">Produk</span>
                        </a>
                        <a title="Penjual" href="<?= base_url("penjualan"); ?>" class="list-group-item list-group-item-action <?= ($subpage == "penjualan" || $subpage == "penjualan_detail") ? 'active' : ''; ?>">
                            <i class="fas fa-cart-arrow-down fa-fw"></i> <span class="d-block small">Penjualan</span>
                        </a>
                        <a title="Zonasi COD" href="<?= base_url("toko/cod"); ?>" class="list-group-item list-group-item-action <?= ($subpage == "cod" || $subpage == "cod_aksi") ? 'active' : ''; ?>">
                            <i class="fas fa-truck-moving fa-fw"></i> <span class="d-block small">Ongkir COD</span>
                        </a>
                    </ul>
                </div>
            </nav>
        </div>

        <div class="col-12 shadow bg-light">
            <?php $this->load->view('frontend/toko/' . $subpage);
            ?>
        </div>
    </div>
</div>