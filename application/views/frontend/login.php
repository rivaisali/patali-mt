<?php $this->load->view('frontend/inc/head_html'); ?>
<div id="page-content">
	<div class="text-center mt-3 d-block d-md-none">
		<!-- <h1 class="text-primary">SIPARDI</h1> -->
		<img src="<?= base_url("assets/img/top-logo.png"); ?>" alt="Logo SIPARDI" class="" width="100">
	</div>
	<div class="container mb-2">
		<div class="row row-cols-1 row-cols-md-2 align-bottom mt-5">
			<div class="col d-none d-md-block text-center">
				<img src="<?= base_url('assets/img/login-img.png'); ?>" class="img-fluid" alt="SIPARDI" width="">
			</div>
			<div class="col">
				<div class="card border shadow">
					<div class="card-body my-1">
						<div id="responseDiv">
							<div id="message"></div>
						</div>
						<h4 class="text-center">Masuk</h4>
						<span class="text-center text-muted d-block">Belum punya akun Sipardi? <a href="<?= base_url('register'); ?>">Daftar.</a></span>
						<form class="mt-3 px-3" method="POST" id="logForm" autocomplete="onn">
							<div class="form-group">
								<label for="email">Email address</label>
								<input type="email" class="form-control" id="email" placeholder="name@example.com" name="email" required autofocus>
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
								<span toggle="#password" class="text-primary fas fa-eye field-icon toggle-password"></span>
							</div>
							<div class="form-group">
								<small class="d-block text-muted text-right"><a href="<?= base_url("forget"); ?>">Lupa Katasandi</a></small>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-outline-primary btn-block" id="logText">Masuk</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('frontend/inc/footer'); ?>
<?php $this->load->view('frontend/inc/foot_html'); ?>