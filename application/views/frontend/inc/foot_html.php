<!-- <script src="<?= base_url('assets/js/jquery-3.5.1.min.js'); ?>"></script> -->
<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="<?= base_url('assets/js/popper.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<!-- owl carousel -->
<script src="<?= base_url('assets/js/owl.carousel.min.js'); ?>"></script>
<!-- datepicker -->
<script src="<?= base_url('assets/js/bootstrap-datepicker.min.js'); ?>"></script>
<!-- datatable -->
<script src="<?= base_url('assets/datatables/datatables.min.js'); ?>"></script>
<!-- select2 -->
<script src="<?= base_url('assets/js/select2.min.js'); ?>"></script>
<!-- ckeditor -->
<script src="//cdn.ckeditor.com/4.14.1/basic/ckeditor.js"></script>
<!-- Slick -->
<script src="<?= base_url('assets/slick/slick.js'); ?>"></script>
<!-- <script src="https://cdn.jsdelivr.net/jquery.slick/1.5.5/slick.min.js"></script> -->

<!-- autocomplete -->
<script src="<?= base_url('assets/js/bootstrap3-typeahead.min.js'); ?>"></script>

<!-- scrollto -->
<script src="<?= base_url('assets/js/jquery.scrollTo.min.js'); ?>"></script>

<!-- timepicker -->
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<script src="<?= base_url('assets/js/custom/my.js'); ?>"></script>

<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>
    // Enable pusher logging - don't include this in production
</script>
<script src="<?= base_url('assets/js/custom/notifikasi.js'); ?>"></script>
</body>

</html>