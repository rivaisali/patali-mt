<a class="nav-link" title="Notifikasi" href="#" id="navbarDropdownNotifikasi" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fas fa-bell fa-fw position-relative">
        <?php if (notifikasiCount() > "0") : ?>
            <span class="badge badge-danger position-absolute rounded-circle" id="badgeNotifikasi">
                <?= notifikasiCount(); ?>
            </span>
        <?php endif; ?>
    </i>
    <span class="d-inlin-block d-lg-none"> Notifikasi <?= user("id_konsumen"); ?></span>
</a>
<div class="dropdown-menu p-0 dropdown-menu-right " aria-labelledby="navbarDropdownNotifikasi">
    <div class="dropdown-item p-0">
        <?php
        if (notifikasiCount() > "0") {
            $notifikasiList = notifikasiList();
        ?>
            <div class="list-group p-0">
                <?php foreach ($notifikasiList as $nl) : ?>
                    <a href="<?= base_url("notifikasi/baca/" . $nl->notifikasi_id); ?>" class="border-left-0 border-top-0 border-right-0 border-primary list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex justify-content-between">
                            <h6 class="mb-1"><?= $nl->judul; ?></h6>
                            <small><?= time_elapsed_string($nl->create_at); ?></small>
                        </div>
                        <p class="mb-1 text-muted small"><?= $nl->pesan; ?></p>
                    </a>
                <?php endforeach; ?>
                <a href="<?= base_url("notifikasi"); ?>" class="border-0 list-group-item list-group-item-action flex-column align-items-start">
                    <p class="mb-1 text-muted small">Lihat Semua</p>
                </a>
            </div>
        <?php } else {
        ?>
            <p class="dropdown-item-text">Belum ada notifikasi.</p>
        <?php
        } ?>
    </div>
</div>