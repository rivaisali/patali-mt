<div class="container mt-3">
    <div class="row">
        <div class="col-md-8 col-12 mb-2">
            <!-- barang -->
            <div class="card checkout">
                <div class="card-header bg-primary text-white mb-3">
                    Produk
                </div>
                <?php
                $total_produk = [];
                $no = 1;
                foreach ($keranjang as $k) :
                    $produk = ambil_data_by_id_row("produk", "id_produk", $k->id_produk);
                    $satuan = ambil_nama_by_id("produk", "satuan", "id_produk", $k->id_produk);
                    $berat = ambil_nama_by_id("produk", "berat", "id_produk", $k->id_produk);
                    $total_produk[$no++] = $k->total;
                ?>
                    <div class="row no-gutters bg-light position-relative">
                        <div class="col-md-3 mb-md-0 p-md-4">
                            <img src="<?= base_url('uploads/produk/' . $produk->gambar); ?>" class="w-100" alt="gambar produk">
                        </div>
                        <div class="col-md-9 position-static p-4 pl-md-0">
                            <h5 class="mt-0"><?= $produk->nama_produk; ?></h5>
                            <b class="d-block"><?= rupiah($k->total); ?></b>
                            <small class="d-block">
                                <?= $k->qty . " " . $satuan; ?>
                                <?= "(" . $berat . " Gram)"; ?>
                            </small>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-4 col-12 mb-2">
            <div class="card border-0">
                <div class="card-body">
                    <div class="d-block mb-3">
                        Total Harga barang
                        <!--<b class="d-block mb-2"><?= rupiah(array_sum($total_produk)); ?></b>-->
                        <b class="d-block mb-2">
                            <?php if (temp('jenis_promo') == "produk") : ?>
                                <del class="mr-2">
                                    <?= rupiah(array_sum($total_produk)); ?>
                                </del>
                                <?= rupiah(array_sum($total_produk) - temp('potongan_barang')); ?>
                            <?php else : ?>
                                <?= rupiah(array_sum($total_produk)); ?>
                            <?php endif; ?>
                            <!-- <?= rupiah(array_sum($total_produk)); ?> -->
                        </b>

                        <!--Ongkos Pengantaran-->
                        <!--<b class="d-block mb-2"><?= rupiah($ongkir); ?></b>-->

                        <!--Total Bayar-->
                        <!--<h5 class="d-block mb-2 font-weight-bold text-primary"><?= rupiah($ongkir + array_sum($total_produk)); ?></h5>-->
                        Ongkos Pengantaran
                        <b class="d-block mb-2">
                            <?php if (temp('jenis_promo') == "ongkir") : ?>
                                <del class="mr-2">
                                    <?= rupiah($ongkir); ?>
                                </del>
                                <?= rupiah($ongkir - temp('potongan_ongkir')); ?>
                            <?php else : ?>
                                <?= rupiah($ongkir); ?>
                            <?php endif; ?>
                        </b>

                        Promo
                        <b class="d-block mb-2">
                            <?php if (temp('status_promo') != "2") : ?>
                                <?= temp('kode_promo'); ?><br>
                                <span class="text-muted small"><?= temp('msg_promo'); ?></span>
                            <?php else : echo '-';
                            endif; ?>
                        </b>

                        Total Bayar
                        <h5 class="d-block mb-2 font-weight-bold text-primary">
                            <?=
                                rupiah(((temp('status_promo') == "1") ? $ongkir - temp('potongan_ongkir') : $ongkir) + array_sum($total_produk) - ((temp('status_promo') == "1") ? temp('potongan_barang') : 0));
                            ?>
                        </h5>
                    </div>

                    <div class="d-block mb-3">
                        <b>Lokasi Pengantaran</b>
                        <address class="d-block mb-2">
                            <?= temp("alamat"); ?><br>
                            <?php $lokasi = ambil_data_by_id_row("rb_desa", "desa_id", temp("kelurahan")); ?>
                            <?= $lokasi->nama_desa . ", " . get_kecamatan($lokasi->kecamatan_id) . ", " . get_kota($lokasi->kota_id); ?>
                        </address>
                    </div>

                    <div class="d-block mb-3">
                        Keterangan
                        <b class="d-block mb-2">
                            <?= (temp("keterangan") != "") ? temp("keterangan") : '-'; ?>
                        </b>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 mb-2">
            <div class="card border-0">
                <div class="card-body">
                    <div class="d-block mb-1">
                        <span class="d-block font-weight-bolder">Pembeli</span>
                        <hr class="my-1">
                        <b class="d-block"><?= $pembeli->nama_lengkap; ?> </b>(<?= $pembeli->no_hp; ?>)<br>
                        <span class="d-block"><?= $pembeli->alamat_lengkap; ?></span>
                        <small class="d-inline-block"><?= get_kelurahan($pembeli->kelurahan); ?>,</small>
                        <small class="d-inline-block"><?= get_kecamatan($pembeli->kecamatan); ?>,</small>
                        <small class="d-inline-block"><?= get_kota($pembeli->kota_id); ?></small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 mb-2">
            <div class="card border-0">
                <div class="card-body">
                    <div class="d-block mb-1">
                        <span class="d-block font-weight-bolder">Penjual</span>
                        <hr class="my-1">
                        <b class="d-block"><?= $penjual->nama_penjual; ?></b>(<?= $penjual->no_telpon; ?>)<br>
                        <span class="d-block"><?= $penjual->alamat_lengkap; ?></span>
                        <small class="d-inline-block"><?= get_kelurahan($penjual->kelurahan); ?>,</small>
                        <small class="d-inline-block"><?= get_kecamatan($penjual->kecamatan); ?>,</small>
                        <small class="d-inline-block"><?= get_kota($penjual->kota_id); ?></small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 mb-3">
            <div class="card border-0">
                <div class="card-body">
                    <a href="<?= base_url('keranjang/buat_pesanan'); ?>" class="btn btn-outline-primary btn-lg btn-block">
                        <i class="fas fa-check fa-fw"></i>
                        Buat Pesanan
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>