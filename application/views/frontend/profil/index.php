<div class="container mt-3">
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-2">
            <nav class="navbar-expand-lg">
                <button class="navbar-toggler btn-block py-3 bg-primary text-white mb-2" type="button" data-toggle="collapse" data-target="#menuProfil" aria-controls="menuProfil" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fas fa-bars"></span> Menu Profil
                </button>
                <div class="collapse navbar-collapse p-0" id="menuProfil">
                    <ul class="list-group w-100">
                        <a href="<?= base_url("profil"); ?>" class="list-group-item list-group-item-action <?= ($subpage == "profil" || $subpage == "edit") ? 'active' : ''; ?>">
                            <i class="fas fa-user fa-fw"></i> Profil Saya
                        </a>
                        <a href="<?= base_url("pembelian"); ?>" class="list-group-item list-group-item-action <?= ($subpage == "pembelian" || $subpage == "pembelian_detail") ? 'active' : ''; ?>">
                            <i class="fas fa-shopping-cart"></i> Pembelian
                        </a>
                        <a href="<?= base_url("point"); ?>" class="list-group-item list-group-item-action <?= ($subpage == "point") ? 'active' : ''; ?>">
                            <i class="fas fa-wallet"></i> Point
                            <span class="badge badge-primary float-right">
                                <?= rupiah(get_point_now()); ?>
                            </span>
                        </a>

                        <?php if (empty($this->session->userdata("penjual"))) { ?>
                            <a href="<?= base_url("profil/daftar_penjual"); ?>" class="mt-5 list-group-item list-group-item-action bg-success text-white">
                                <i class="fas fa-store fa-fw"></i>
                                Mendaftar Sebagai Penjual
                            </a>
                        <?php } ?>
                    </ul>


                </div>
            </nav>
        </div>

        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 shadow bg-light">
            <?php $this->load->view('frontend/profil/' . $subpage); ?>
        </div>
    </div>
</div>