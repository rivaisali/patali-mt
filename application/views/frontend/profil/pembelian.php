<div class="row">
    <div class="col-12">
        <ul class="nav nav-pills nav-fill mt-2">
            <li class="nav-item">
                <a title="Menunggu Verifikasi Penjual" class="nav-link <?= ($status == "menunggu") ? 'active' : ''; ?>" href="<?= base_url("profil/pembelian"); ?>"><i class="fas fa-clock fa-fw"></i> <span class="d-none d-sm-block">Menunggu</span></a>
            </li>
            <li class="nav-item">
                <a title="Sedang Di Kemas" class="nav-link <?= ($status == "kemas") ? 'active' : ''; ?>" href="<?= base_url("profil/pembelian/kemas"); ?>"><i class="fas fa-box-open fa-fw"></i> <span class="d-none d-sm-block">Dikemas</span></a>
            </li>
            <li class="nav-item">
                <a title="Sedang Diantar" class="nav-link <?= ($status == "antar") ? 'active' : ''; ?>" href="<?= base_url("profil/pembelian/antar"); ?>"><i class="fas fa-truck fa-fw"></i> <span class="d-none d-sm-block">Diantar</span></a>
            </li>
            <li class="nav-item">
                <a title="Selesai" class="nav-link <?= ($status == "selesai") ? 'active' : ''; ?>" href="<?= base_url("profil/pembelian/selesai"); ?>"><i class="fas fa-check-circle fa-fw"></i> <span class="d-none d-sm-block">Selesai</span></a>
            </li>
            <li title="Dibatalkan" class="nav-item">
                <a class="nav-link <?= ($status == "batal") ? 'active' : ''; ?>" href="<?= base_url("profil/pembelian/batal"); ?>"><i class="fas fa-times-circle fa-fw"></i> <span class="d-none d-sm-block">Dibatalkan</span></a>
            </li>
        </ul>
        <hr>
        <?php
        foreach ($pembelian as $pem) :
            $id_produk = ambil_nama_by_id("rb_penjualan_detail", "id_produk", "id_penjualan", $pem->id_penjualan);
            $produk = ambil_data_by_id_row("produk", "id_produk", $id_produk);
        ?>
            <a class="text-decoration-none" href="<?= base_url("pembelian/detail/" . url_title(strtolower($pem->kode_transaksi))); ?>">
                <div class="text-dark card mb-3 overflow-hidden p-0" style="">
                    <div class="row no-gutters h-100">
                        <div class="col-md-4">
                            <div class="thumbnail">
                                <img src="<?= base_url("uploads/produk/" . $produk->gambar); ?>" class="card-img">
                            </div>
                        </div>
                        <div class="col-md-8 col-12">
                            <div class="card-body">
                                <h5 class="card-title">Kode Pembelian : <?= $pem->kode_transaksi; ?></h5>
                                <p class="card-text">
                                    Penjual : <?= ambil_nama_by_id("penjual", "nama_penjual", "id_penjual", $pem->id_penjual); ?><br>
                                    Kode Promo : <?= $pem->kode_promo; ?>
                                    <span class="d-block font-weight-bold">Total Bayar : <?= rupiah($pem->total_produk + $pem->ongkir - (($pem->kode_promo != "") ? $pem->potongan_ongkir + $pem->potongan_barang : 0)); ?></span>
                                    <!--<span class="d-block font-weight-bold">Total Bayar : <?= rupiah($pem->total_produk + $pem->ongkir); ?></span>-->
                                </p>
                                <p class="card-text">
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <i class="fas fa-tags fa-fw"></i> <?= rupiah($pem->total_produk); ?>
                                        </li>
                                        <li class="list-inline-item">
                                            <i class="fas fa-truck fa-fw"></i> <?= rupiah($pem->ongkir); ?>
                                        </li>
                                        <li class="list-inline-item">
                                            <i class="fas fa-clock fa-fw"></i> <?= time_elapsed_string($pem->waktu_transaksi); ?>
                                        </li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        <?php endforeach ?>
        <div class="row">
            <div class="col-12 mt-3">
                <!--Tampilkan pagination-->
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
</div>