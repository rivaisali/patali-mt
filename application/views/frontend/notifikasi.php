<div class="container my-3">
    <div class="list-group">
        <?php foreach ($data as $d) : ?>
            <a href="<?= base_url("notifikasi/baca/" . $d->notifikasi_id); ?>" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1"><?= $d->judul; ?></h5>
                    <small><?= time_elapsed_string($d->create_at); ?></small>
                </div>
                <p class="mb-1"><?= $d->pesan; ?></p>
                <small>Dari: <?= $d->dari; ?>.</small>
            </a>
        <?php endforeach; ?>
    </div>
</div>