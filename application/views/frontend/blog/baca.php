<section class="pt-5 pb-5">
    <div class="container">
        <div class="row">

            <!-- Post Content Column -->
            <div class="col-lg-8">

                <!-- Title -->
                <h1 class="mt-4"><?= $data->judul; ?></h1>

                <!-- Author -->
                <p class="lead">
                    by
                    <a href="#"><?= $data->username; ?></a>
                </p>

                <hr>

                <!-- Date/Time -->
                <p>Diposting <?= $data->hari . ', ' . tgl_indonesia($data->tanggal) . ', ' . substr($data->jam, 0, 5); ?></p>

                <hr>

                <!-- Preview Image -->
                <img class="img-fluid rounded" src="https://be.sipardi.id<?= $data->gambar; ?>" alt="">

                <hr>

                <!-- Post Content -->
                <?= $data->isi_berita; ?>

                <hr>
                <!-- Comments Form -->
                <!-- <div class="card my-4">
                    <h5 class="card-header">Leave a Comment:</h5>
                    <div class="card-body">
                        <form>
                            <div class="form-group">
                                <textarea class="form-control" rows="3"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div> -->
            </div>

            <!-- Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Search Widget -->
                <!-- <div class="card my-4">
                    <h5 class="card-header">Search</h5>
                    <div class="card-body">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="button">Go!</button>
                            </span>
                        </div>
                    </div>
                </div> -->

                <!-- Categories Widget -->
                <!-- <div class="card my-4">
                    <h5 class="card-header">Categories</h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="#">Web Design</a>
                                    </li>
                                    <li>
                                        <a href="#">HTML</a>
                                    </li>
                                    <li>
                                        <a href="#">Freebies</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="#">JavaScript</a>
                                    </li>
                                    <li>
                                        <a href="#">CSS</a>
                                    </li>
                                    <li>
                                        <a href="#">Tutorials</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> -->

                <!-- Side Widget -->
                <div class="card my-4">
                    <h5 class="card-header">Blog Lainnya</h5>
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <?php
                            foreach ($others as $d) :
                            ?>
                                <li class="row mb-4">
                                    <a href="<?= base_url('blog/' . $d->judul_seo); ?>" class="col-3">
                                        <img src="https://be.sipardi.id<?= $d->gambar; ?>" alt="Image" class="rounded img-fluid">
                                    </a>
                                    <div class="col-9">
                                        <a href="<?= base_url('blog/' . $d->judul_seo); ?>">
                                            <h6 class="mb-3 h5 text-dark text-truncate text-decoration-none"><?= $d->judul; ?></h6>
                                        </a>
                                        <div class="d-flex text-small">
                                            <span class="text-muted"><?= $d->hari . ', ' . tgl_indonesia($d->tanggal) . ', ' . substr($d->jam, 0, 5); ?></span>
                                        </div>
                                    </div>
                                </li>
                            <?php
                            endforeach; ?>
                        </ul>
                    </div>
                </div>

            </div>

        </div>
        <!-- /.row -->
    </div>
</section>