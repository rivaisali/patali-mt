<section class="pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 mb-4">
                <?php
                foreach ($data as $ind => $d) :
                    if ($ind == "0") :
                ?>
                        <img class="img-fluid rounded mb-3" src="https://be.sipardi.id<?= $d->gambar; ?>" alt="A guide to building your online presence">
                        <a href="<?= base_url('blog/' . $d->judul_seo); ?>" class="my-0 h2 text-dark text-truncate d-block text-decoration-none"><?= $d->judul; ?></a>
                        <p class="mt-3">
                            <?= substr(strip_tags($d->isi_berita), 0, 250); ?>
                        </p>
                        <div class="d-flex text-small">
                            <!-- <a href="#">Business</a> -->
                            <span class="text-muted"><?= $d->hari . ', ' . tgl_indonesia($d->tanggal) . ', ' . substr($d->jam, 0, 5); ?></span>
                        </div>
                <?php endif;
                endforeach; ?>
            </div>
            <div class="col-lg-5 ">
                <ul class="list-unstyled">
                    <?php
                    foreach ($data as $ind => $d) :
                        if ($ind > "0") :
                    ?>
                            <li class="row mb-4">
                                <a href="<?= base_url('blog/' . $d->judul_seo); ?>" class="col-3">
                                    <img src="https://be.sipardi.id<?= $d->gambar; ?>" alt="Image" class="rounded img-fluid">
                                </a>
                                <div class="col-9">
                                    <a href="<?= base_url('blog/' . $d->judul_seo); ?>">
                                        <h6 class="mb-3 h5 text-dark text-truncate text-decoration-none"><?= $d->judul; ?></h6>
                                    </a>
                                    <div class="d-flex text-small">
                                        <span class="text-muted"><?= $d->hari . ', ' . tgl_indonesia($d->tanggal) . ', ' . substr($d->jam, 0, 5); ?></span>
                                    </div>
                                </div>
                            </li>
                    <?php endif;
                    endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</section>