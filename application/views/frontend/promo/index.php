<div class="container">
    <?php if (empty($data)) echo '<div class="alert alert-success mt-2">Belum ada promo.</div>'; ?>
    <div class="row row-cols-1 row-cols-md-3 mt-2">

        <?php $no = 1;
        foreach ($data as $d) :
            if ($d->gambar == "") {
                $gambar =   base_url("uploads/promo/default.png");
            } else {
                $gambar =   "https://be.sipardi.id/$d->gambar";
            }
        ?>
            <div class="col mb-2">
                <div class="card h-100">
                    <img class="card-img-top" src="<?= $gambar; ?>" alt="Card image cap">
                    <div class="card-body">
                        <p><?= $d->nama_promo; ?></p>
                        <dl class="row mb-0">
                            <dt class="col-1"><i class="fa fa-barcode"></i></dt>
                            <dd class="col-10"><?= $d->kode_referal; ?></dd>
                            <input type="hidden" name="promo" id="kode<?= $no; ?>" value="<?= $d->kode_referal; ?>">
                        </dl>
                        <small>
                            <a href="javascript:void(0)" class="salinKodePromo" data-trigger="kode<?= $no; ?>">Salin Kode</a>
                        </small>
                    </div>
                    <div class="card-footer text-muted">
                        <a href="javascript:void(0)" class="btn btn-success btn-sm btn-block syaratPromo" data-title="<?= $d->nama_promo; ?>" data-keterangan="<?= $d->keterangan; ?>" data-gambar="<?= $gambar; ?>" data-sdk="<?= $d->syarat_ketentuan; ?>">Syarat dan Ketentuan</a>
                    </div>
                </div>
            </div>
        <?php $no++;
        endforeach; ?>
    </div>
</div>