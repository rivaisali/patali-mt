<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function konversi_harga($n, $precision = 1)
{
	if ($n < 900) {
		// 0 - 900
		$n_format = number_format($n, $precision);
		$suffix = '';
	} else if ($n < 900000) {
		// 0.9k-850k
		$n_format = number_format($n / 1000, $precision);
		$suffix = ' Rb';
	} else if ($n < 900000000) {
		// 0.9m-850m
		$n_format = number_format($n / 1000000, $precision);
		$suffix = ' Jt';
	} else if ($n < 900000000000) {
		// 0.9b-850b
		$n_format = number_format($n / 1000000000, $precision);
		$suffix = ' Ml';
	} else {
		// 0.9t+
		$n_format = number_format($n / 1000000000000, $precision);
		$suffix = ' Tr';
	}
	// Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
	// Intentionally does not affect partials, eg "1.50" -> "1.50"
	if ($precision > 0) {
		$dotzero = '.' . str_repeat('0', $precision);
		$n_format = str_replace($dotzero, '', $n_format);
	}
	return $n_format . $suffix;
}

// ubah string status ke angka status
function status_string_to_num($str)
{
	switch ($str) {
		case "menunggu":
			$num = "1";
			break;
		case "kemas":
			$num = "2";
			break;
		case "antar":
			$num = "3";
			break;
		case "selesai":
			$num = "4";
			break;
		case "batal":
			$num = "0";
			break;
		default:
			$num = "1";
			break;
	}

	return $num;
}

// ubah string status ke number
function status_num_to_string($num)
{
	switch ($num) {
		case "1":
			$str = "menunggu";
			break;
		case "2":
			$str = "kemas";
			break;
		case "3":
			$str = "antar";
			break;
		case "4":
			$str = "selesai";
			break;
		case "0":
			$str = "batal";
			break;
		default:
			$str = "menunggu";
			break;
	}

	return $str;
}

function apk($index)
{
	$ci = &get_instance();
	$ret	=	$ci->session->userdata("data_aplikasi")[$index];
	return $ret;
}

function user($index)
{
	$ci = &get_instance();
	$ret	=	$ci->session->userdata("user")[$index];
	return $ret;
}

function penjual($index)
{
	$ci = &get_instance();
	if (!$ci->session->userdata("penjual")) {
		$ret = "0";
	} else {
		$ret = $ci->session->userdata("penjual")[$index];
	}
	// $ret	=	$ci->session->userdata("penjual")[$index];
	return $ret;
}

function temp($index)
{
	$ci = &get_instance();
	$ret	=	$ci->session->userdata("temporary")[$index];
	return $ret;
}

function cari($index)
{
	$ci = &get_instance();
	// $ret	=	$ci->session->userdata("cari")[$index];
	if (!$ci->session->userdata("cari")) {
		$ret = "";
	} else {
		$ret = $ci->session->userdata("cari")[$index];
	}
	return $ret;
}

function cari_filter($index)
{
	$ci = &get_instance();
	$ret	=	$ci->session->userdata("cari_filter")[$index];
	return $ret;
}

function cari_penjual($index)
{
	$ci = &get_instance();
	if (!$ci->session->userdata("cari_penjual")) {
		$ret = "";
	} else {
		// $ret = $ci->session->userdata("penjual")[$index];
		$ret	=	$ci->session->userdata("cari_penjual")[$index];
	}
	return $ret;
}

function rupiah($angka)
{
	$hasil_rupiah = "Rp. " . number_format($angka, 0, ',', '.');
	return $hasil_rupiah;
}

function get_kota($id)
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where("kota_id", $id);
	$CI->db->from("rb_kota");
	$query 	= 	$CI->db->get();
	$data	=	$query->row();
	return ($data) ? $data->nama_kota : '-';
}

function get_kecamatan($id)
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where("kecamatan_id", $id);
	$CI->db->from("rb_kecamatan");
	$query 	= 	$CI->db->get();
	$data	=	$query->row();
	return ($data) ? $data->nama_kecamatan : '-';
}

function get_kelurahan($id)
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where("desa_id", $id);
	$CI->db->from("rb_desa");
	$query 	= 	$CI->db->get();
	$data	=	$query->row();
	return ($data) ? $data->nama_desa : '-';
}

function get_kategori($id)
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where("id_kategori_produk", $id);
	$CI->db->from("rb_kategori_produk");
	$query 	= 	$CI->db->get();
	$data	=	$query->row();
	return ($data) ? $data->nama_kategori : '-';
}

function get_point_now()
{
	$CI = &get_instance();
	$id_konsumen = user("id_konsumen");
	$CI->load->model("crud_model");
	$point_get = $CI->crud_model->select_sum("point_get", "point", ["id_konsumen" => $id_konsumen, "waktu_ekspire >=" => date("Y-m-d")]);
	$point_claim = $CI->crud_model->select_sum("point_claim", "point", ["id_konsumen" => $id_konsumen]);
	return $point_get - $point_claim;
}

// fungsi untuk ambil waktu yang lalu
function time_elapsed_string($datetime, $full = false)
{
	$now = new DateTime;
	$ago = new DateTime($datetime);
	$diff = $now->diff($ago);

	$diff->w = floor($diff->d / 7);
	$diff->d -= $diff->w * 7;

	$string = array(
		'y' => 'tahun',
		'm' => 'bulan',
		'w' => 'minggu',
		'd' => 'hari',
		'h' => 'jam',
		'i' => 'menit',
		's' => 'detik',
	);
	foreach ($string as $k => &$v) {
		if ($diff->$k) {
			$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
		} else {
			unset($string[$k]);
		}
	}

	if (!$full) $string = array_slice($string, 0, 1);
	return $string ? implode(', ', $string) . ' lalu' : 'sekarang';
}

function kirim_email($tujuan, $subject, $pesan)
{
	$ci = &get_instance();
	$ci->load->library('email');
	$ci->email->from("juraganjasagto@gmail.com", "JURAGAN JASA");
	$ci->email->to($tujuan);
	$ci->email->subject($subject);
	$ci->email->message($pesan);
	return $ci->email->send();
}

// convert image
function image_resize($image, $path = "poto_profil", $width = 50, $height = 50)
{
	// Configuration
	$CI = &get_instance();
	$config['image_library'] = 'gd2';
	$config['source_image'] = './uploads/' . $path . '/' . $image;
	$config['new_image'] = './uploads/' . $path . '/thumb_' . $image;
	// $config['create_thumb'] = TRUE;
	$config['maintain_ratio'] = FALSE;
	$config['width'] = $width;
	$config['height'] = $height;
	// Load the Library
	$CI->load->library('image_lib', $config);
	// resize image
	$CI->image_lib->resize();
	// handle if there is any problem
	if (!$CI->image_lib->resize()) {
		echo $CI->image_lib->display_errors();
	}
}

function generateRandomString($length = 10)
{
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

if (!function_exists('getListKategori')) {
	function getListKategori($id = NULL, $return = TRUE)
	{
		$ci = &get_instance();
		$ci->load->model("crud_model");
		$items	=	$ci->crud_model->select_by_field("rb_kategori_produk", "id_kategori_produk, nama_kategori", ["id_parent" => $id]);
		$ret = "<ul ";
		if ($id !== null) {
			$ret .= 'class="submenu dropdown-menu menu-kategori dropdown-kategori"';
		} else {
			$ret .= 'class="dropdown-menu menu-kategori dropdown-kategori"';
		}
		$ret .= ">";
		foreach ($items as $item) {
			if (empty($item)) continue;
			// $has_sub = isset($item[$options['submenu_key']]) && !empty($item[$options['submenu_key']]);
			$has_sub = $ci->crud_model->cek_data_where_array("rb_kategori_produk", ["id_parent" => $item->id_kategori_produk]);
			$ret .= "<li><a class='dropdown-item' href='produk?keyword=&kategori=" . $item->id_kategori_produk . "&sortir=&cari=Cari'>" . ucwords(strtolower($item->nama_kategori)) . "</a>";
			// $ret .= "<br><span class='label label-primary'>asd</span>";
			if (!$has_sub)
				$ret .= getListKategori($item->id_kategori_produk);
			$ret .= '</li>';
		}
		$ret .= '</ul>';
		if ($return) return $ret;
		else echo $ret;
	}
}

// get no hp
function hp($nohp)
{
	// kadang ada penulisan no hp 0811 239 345
	$nohp = str_replace(" ", "", $nohp);
	// kadang ada penulisan no hp (0274) 778787
	$nohp = str_replace("(", "", $nohp);
	// kadang ada penulisan no hp (0274) 778787
	$nohp = str_replace(")", "", $nohp);
	// kadang ada penulisan no hp 0811.239.345
	$nohp = str_replace(".", "", $nohp);

	// cek apakah no hp mengandung karakter + dan 0-9
	if (!preg_match('/[^+0-9]/', trim($nohp))) {
		// cek apakah no hp karakter 1-3 adalah +62
		if (substr(trim($nohp), 0, 3) == '+62') {
			$hp = trim($nohp, '+');
		} elseif (substr(trim($nohp), 0, 2) == '62') {
			$hp = trim($nohp);
		}

		// cek apakah no hp karakter 1 adalah 0
		elseif (substr(trim($nohp), 0, 1) == '0') {
			$hp = '62' . substr(trim($nohp), 1);
		} else {
			$hp = trim($nohp);
		}
	}
	return $hp;
}

function notifikasiCount()
{
	$ci = &get_instance();
	$ci->load->model("crud_model");
	$where  =	[
		"tujuan"	=>	user("id_konsumen"),
		"status"	=>	"0"
	];
	$items	=	$ci->crud_model->select_all_where_array_num_row("notifikasi", $where);
	return $items;
}

function notifikasiList()
{
	$ci = &get_instance();
	$ci->load->model("crud_model");
	$where  =	[
		"tujuan"	=>	user("id_konsumen"),
		"status"	=>	"0"
	];
	$items	=	$ci->crud_model->select_all_where_array_limit("notifikasi", $where, "create_at", "DESC", "5");
	return $items;
}

function cekPopUp()
{
	$ci = &get_instance();
	// $iklan = $ci->crud_model->select_one('identitas', 'id_identitas', '1');
	$iklan = $ci->crud_model->select_one('tbl_iklan', 'is_default', '1');
	if (!empty($iklan)) {
		if ($ci->session->userdata('iklan') || $ci->session->userdata('iklan') == "Y") {
			return FALSE;
		} else {
			return TRUE;
		}
	} else {
		return FALSE;
	}
}

// cek waktu operasional
function cek_operasional($id)
{
	$ci = &get_instance();
	$penjual = $ci->crud_model->select_one('penjual', 'id_penjual', $id);
	if ($penjual->buka >= date("H:i:s") || $penjual->tutup <= date("H:i:s")) {
		return true;
	} else {
		return false;
	}
}
