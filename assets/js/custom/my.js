// show password
$(".toggle-password").click(function () {
	$(this).toggleClass("fa-eye fa-eye-slash");
	var input = $($(this).attr("toggle"));
	if (input.attr("type") == "password") {
		input.attr("type", "text");
	} else {
		input.attr("type", "password");
	}
});

$(document).ready(function () {
	cekKeranjang();

	$(".baner").slick({
		// dots: true,
		// arrows: true,
		autoplay: true,
		centerMode: true,
		centerPadding: '2',
		slidesToShow: 2,
		slidesToScroll: 2,
		responsive: [{
			breakpoint: 768,
			settings: {
				arrows: false,
				slidesToScroll: 1,
				slidesToShow: 1,
			}
		}]
	});

	$(".owl-carousel").owlCarousel({
		center: true,
		startPosition: 0,
		items: 1,
		loop: true,
		margin: 50,
		autoplay: true,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1.5
			}
		}
	});

	$(".other-product").owlCarousel({
		center: true,
		startPosition: 0,
		items: 1,
		loop: true,
		margin: 50,
		autoplay: true,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1.5
			}
		}
	});

	// var owl = $('.owl-carousel');
	// owl.owlCarousel({
	// 	// onChange: scale,
	// 	center: true,
	// 	startPosition: 0,
	// 	items: 1,
	// 	loop: true,
	// 	margin: 50,
	// 	autoplay: true,
	// 	responsive: {
	// 		0: {
	// 			items: 1
	// 		},
	// 		600: {
	// 			items: 1.5
	// 		}
	// 	}
	// });

	$('.datepicker').datepicker({
		'autoclose': true,
		'format': "yyyy-mm-dd",
		'defaultViewDate': '1990-01-01'
	});

	$('#datatable').DataTable({
		responsive: true,
		columnDefs: [{
				responsivePriority: 1,
				targets: 0
			},
			{
				responsivePriority: 2,
				targets: 1
			}
		]
	});

	$('#datatable2').DataTable({
		responsive: true,
		columnDefs: [{
				responsivePriority: 1,
				targets: 0
			},
			{
				responsivePriority: 2,
				targets: 1
			}
		]
	});

	$('.kategoriProduk').select2({
		theme: 'bootstrap4',
		ajax: {
			url: url + '/ajax/ambil_kategori',
			dataType: 'json',
			delay: 250,
			type: "post",
			data: function (params) {
				return {
					key: params.term,
				};
			},
			processResults: function (response) {
				return {
					results: response
				};
			},
			cache: true
		},
		placeholder: 'Pilih Kategori Produk',
		allowClear: Boolean($(this).data('allow-clear')),
		templateResult: formatKategori,
		templateSelection: formatKategoriSelection
	});

	function formatKategori(result) {
		var $container = $(
			"<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__meta'>" +
			"<div class='select2-result-repository__title font-weight-bold'><span class='select2-result-repository__description text-lowercase text-muted font-italic'></span></div>" +
			"</div>" +
			"</div>"
		);

		$container.find(".select2-result-repository__title").text(result.text);
		$container.find(".select2-result-repository__description").text(result.description);
		return $container;
	}

	function formatKategoriSelection(result) {
		return result.text;
	}

	const deskripsi = document.getElementById("deskripsi");
	if (deskripsi) {
		CKEDITOR.replace('deskripsi');
	}

});

function cekKeranjang() {
	const badge = document.getElementById("badgeKeranjang");
	if (badge) {
		badge.innerText = "";
		$.ajax({
			url: url + '/ajax/cek_keranjang',
			dataType: 'json',
			success: function (success) {
				console.log(success);
				if (success.status == "1") {
					badge.innerText = success.data;
				} else {
					badge.innerText = "";
				}
			}
		})
	}
}


$(document).ready(function () {
	$('.container-beranda').on('mouseover', '.product', function () {
		$(this).addClass('shadow-product');
		// console.log($(this));
	})

	$('.container-beranda').on('mouseleave', '.product', function () {
		$(this).removeClass('shadow-product');
		// console.log($(this));
	});

	$('#container-produk').on('mouseover', '.product', function () {
		$(this).addClass('shadow-product');
		// console.log($(this));
	})

	$('#container-produk').on('mouseleave', '.product', function () {
		$(this).removeClass('shadow-product');
		// console.log($(this));
	});

	$('#container-penjual').on('mouseover', '.penjual', function () {
		$(this).addClass('shadow-seller');
	})

	$('#container-penjual').on('mouseleave', '.penjual', function () {
		$(this).removeClass('shadow-seller');
	})
})

$(document).scroll(function () {
	var sidebarFilter = $("#filter");
	// sidebarFilter.toggleClass('fix-filter', $(this).scrollTop() > sidebarFilter.height());
	sidebarFilter.toggleClass('fix-filter', $(this).scrollTop() > 100);
});

// button login
$(document).ready(function () {
	$('#logForm').submit(function (e) {
		e.preventDefault();
		$('#logText').html('<i class="fas fa-spinner fa-spin"></i>&nbsp; Checking...');
		var user = $('#logForm').serialize();
		var login = function () {
			$.ajax({
				type: 'POST',
				url: url + '/login/do_login',
				dataType: 'json',
				data: user,
				success: function (response) {
					$('#message').html(response.message);
					$('#logText').html('Login');
					if (response.error) {
						$('#responseDiv').removeClass('alert alert-success').addClass('alert alert-danger').show();
						$('#email').addClass("is-invalid");
						$('#password').addClass("is-invalid");
					} else {
						$('#responseDiv').removeClass('alert alert-danger').addClass('alert alert-success').show();
						// $('#logForm')[0].reset();
						$('#email').removeClass("is-invalid");
						$('#password').removeClass("is-invalid");
						$('#email').addClass("is-valid");
						$('#password').addClass("is-valid");
						$('.toggle-password').hide();
						setTimeout(function () {
							location.reload();
						}, 3000);
					}
				}
			});
		};
		setTimeout(login, 3000);
	});
})

// validasi hanya angka
function hanyaAngka(evt) {
	var theEvent = evt || window.event;

	// Handle paste
	if (theEvent.type === 'paste') {
		key = event.clipboardData.getData('text/plain');
	} else {
		// Handle key press
		var key = theEvent.keyCode || theEvent.which;
		key = String.fromCharCode(key);
	}
	var regex = /[0-9]|\./;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault) theEvent.preventDefault();
	}
}

// konvert ke rupiah
function convertToRupiah(angka) {
	var rupiah = '';
	var angkarev = angka.toString().split('').reverse().join('');
	for (var i = 0; i < angkarev.length; i++)
		if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
	return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
}

// mengambil jumlah beli dan dikalikan dengan harga kemudian di masukkan kedalam total
const jumlahBeli = document.getElementById("jumlahBeli");
const tambahJumlahBeli = document.getElementById("tambahJumlahBeli");
const kurangJumlahBeli = document.getElementById("kurangJumlahBeli");
const harga = document.getElementById("harga");
const stok = document.getElementById("stok");
const total = document.getElementById("total");
const totalHarga = document.getElementById("totalHarga");

$(document).ready(function () {
	cekTotalHarga();
});

if (jumlahBeli) {
	jumlahBeli.addEventListener("input", function () {
		cekStok();
		cekTotalHarga();
	});
}

// tombol tambah jumlah beli
if (tambahJumlahBeli) {
	tambahJumlahBeli.addEventListener("click", function () {
		let angka;
		if (jumlahBeli.value === "") {
			angka = 1;
		} else {
			angka = parseInt(jumlahBeli.value) + 1;
		}
		jumlahBeli.value = angka;
		cekStok();
		cekTotalHarga();
	});
}

// tombol kurang jumlah beli
if (kurangJumlahBeli) {
	kurangJumlahBeli.addEventListener("click", function () {
		let angka;
		if (jumlahBeli.value === "") {
			angka = 1;
		} else if (jumlahBeli.value <= "1") {
			angka = 1;
		} else {
			angka = parseInt(jumlahBeli.value) - 1;
		}
		jumlahBeli.value = angka;
		cekStok();
		cekTotalHarga();
	});
}

// cek stok
function cekStok() {
	if (parseInt(jumlahBeli.value) > parseInt(stok.value)) {
		alert('Jumlah stok sisa ' + stok.value);
		jumlahBeli.value = stok.value;
		cekTotalHarga();
	}
}

// menampilkan total harga
function cekTotalHarga() {
	if (jumlahBeli) {
		const jumlahBayar = jumlahBeli.value * harga.value;
		totalHarga.innerText = convertToRupiah(jumlahBayar);
		total.value = jumlahBayar;
	}
}

// tambah jumlah beli
$('#keranjang .tambah').on('click', function () {
	// const stok = $('#stok')
	const prev = $(this).prevAll();
	const stok = prev[2].value;
	// console.log(prev[2].value);
	const idKeranjang = prev[1].innerText;
	let qty = parseInt(prev[0].innerText) + 1;
	if (qty > stok) {
		qty = qty - 1;
	}
	const harga = parseInt($(this).parent().prev().data('harga'));
	const total = $(this).parent().next().children()[0];
	const totalValue = qty * harga;
	const qtyPlace = prev[0];
	qtyPlace.innerText = qty;
	total.innerText = convertToRupiah(totalValue);
	$(total).attr('data-total', totalValue);
	// console.log(totalValue);
	$.ajax({
		url: url + 'keranjang/gantiJumlah',
		type: 'post',
		dataType: 'json',
		data: {
			'id_keranjang': idKeranjang,
			'qty': qty,
			'total': totalValue
		},
		success: function (success) {
			console.log(success);
			hitungTotalBayar();
		}
	})
});

// kurangi jumlah beli
$('#keranjang .kurang').on('click', function () {
	const next = $(this).nextAll();
	if (next[2].innerText == "1") {
		alert('Klik tombol sampah di sudut kanan untuk menghapus dari keranjang');
	} else {
		const idKeranjang = next[1].innerText;
		const qty = parseInt(next[2].innerText) - 1;
		const harga = parseInt($(this).parent().prev().data('harga'));
		const total = $(this).parent().next().children()[0];
		const totalValue = qty * harga;
		const qtyPlace = next[2];
		qtyPlace.innerText = qty;
		total.innerText = convertToRupiah(totalValue);
		$(total).attr('data-total', totalValue);
		// console.log(totalValue);
		$.ajax({
			url: url + 'keranjang/gantiJumlah',
			type: 'post',
			dataType: 'json',
			data: {
				'id_keranjang': idKeranjang,
				'qty': qty,
				'total': totalValue
			},
			success: function (success) {
				hitungTotalBayar();
				console.log(success);
			}
		})
	}
});

// hapus dari keranjang belanja
// const keranjang = document.querySelector('#keranjang');
$('#keranjang').on('click', '.hapus', function (e) {
	e.preventDefault();
	const row = (this).parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
	const idKeranjang = (this).getAttribute('id');
	$.ajax({
		url: url + 'keranjang/hapus',
		type: 'post',
		dataType: 'json',
		data: {
			'id_keranjang': idKeranjang
		},
		success: function (success) {
			// row.style.display = 'none';
			row.remove();
			console.log(success);
			hitungTotalBayar();
			cekKeranjang();
			cekIsiKeranjang();
		}
	})
})

function cekIsiKeranjang() {
	$.ajax({
		url: url + 'ajax/cek_keranjang',
		dataType: 'json',
		success: function (success) {
			if (success.status == "0") {
				setTimeout(function () {
					location.reload();
				}, 100);
			}
		}
	})
}

// hitung semua total harga dari item
function hitungTotalBayar() {
	const ongkir = parseInt($('#ongkir').data('ongkir'));
	const subTotal = document.querySelectorAll('.total');
	// const subTotal = $()('.total').data("total");
	let bayar = 0;
	subTotal.forEach(e => {
		bayar = bayar + parseInt(e.getAttribute('data-total'));
	});
	// const ongkir = getOngkir();
	const totalBayar = document.getElementById("totalBayar");
	totalBayar.innerText = convertToRupiah(bayar + ongkir);
}

// const totalBayar = document.getElementById("totalBayar");
// if (totalBayar) {
// 	$(document).ready(function () {
// getOngkir();
// hitungTotalBayar();
// 	});
// }


// =========================================================================================================================
// Kabupaten kota / Kecamatan / Kelurahan
// =========================================================================================================================
$(document).ready(function () {
	const kabKota = document.getElementById("kabkota");
	const kecamatan = document.getElementById("kecamatan");
	const kelurahan = document.getElementById("kelurahan");

	// jika kab kota di ubah
	if (kabKota) {
		kabKota.addEventListener("change", function (e) {
			const idKota = e.target.value;
			$.ajax({
				url: url + 'ajax/ambil_kecamatan',
				type: 'post',
				dataType: 'json',
				data: {
					'kota_id': idKota
				},
				success: function (success) {
					// row.style.display = 'none';
					console.log(success);
					kecamatan.innerHTML = "";
					kelurahan.innerHTML = "";
					if (success.status == 1) {
						kecamatan.innerHTML += '<option selected disabled value="0">Pilih Kecamatan</option>';
						kelurahan.innerHTML += '<option selected disabled value="0">Pilih Kelurahan</option>';
						success.data.forEach(kec => {
							kecamatan.innerHTML += `
								<option value='${kec.kecamatan_id}'>${kec.nama_kecamatan}</option>
							`;
						})
					} else {
						kecamatan.innerHTML += '<option selected disabled value="0">Pilih Kecamatan</option>';
						kelurahan.innerHTML += '<option selected disabled value="0">Pilih Kelurahan</option>';
					}
				}
			})
		})
	}

	// jika kecamatan di ubah
	if (kecamatan) {
		kecamatan.addEventListener("change", function (e) {
			const idKecamatan = e.target.value;
			$.ajax({
				url: url + 'ajax/ambil_desa',
				type: 'post',
				dataType: 'json',
				data: {
					'kecamatan_id': idKecamatan
				},
				success: function (success) {
					// row.style.display = 'none';
					console.log(success);
					kelurahan.innerHTML = "";
					if (success.status == 1) {
						kelurahan.innerHTML += '<option selected disabled value="0">Pilih Kelurahan</option>';
						success.data.forEach(kel => {
							kelurahan.innerHTML += `
							<option value='${kel.desa_id}'>${kel.nama_desa}</option>
							`;
						})
					} else {
						kelurahan.innerHTML += '<option selected disabled value="0">Pilih Kelurahan</option>';
					}
				}
			})
		})
	}
})


// preview foto profil
function previewImg() {
	const foto = document.querySelector("#foto");
	const fotoLabel = document.querySelector('.custom-file-label');
	const imgPreview = document.querySelector('.img-preview');

	fotoLabel.textContent = foto.files[0].name;

	const fileFoto = new FileReader();
	fileFoto.readAsDataURL(foto.files[0]);

	fileFoto.onload = function (e) {
		imgPreview.src = e.target.result;
	}
}

// preview KTP
function previewKtp() {
	const foto = document.querySelector("#ktp");
	const fotoLabel = document.querySelector('.ktp-label');
	const ktpPreview = document.querySelector('.ktp-preview');

	fotoLabel.textContent = foto.files[0].name;

	const fileFoto = new FileReader();
	fileFoto.readAsDataURL(foto.files[0]);

	fileFoto.onload = function (e) {
		ktpPreview.src = e.target.result;
	}
}

// =======================================================================================================================================
// cek ongkir 
// =======================================================================================================================================
const idPenjual = document.querySelector('.id-penjual');
const kelurahanPenjual = document.querySelector('.kelurahan-penjual');
const kelurahanPembeli = document.querySelector('.kelurahan-pembeli');
const kotaPembeli = document.querySelector('.kota-pembeli');

$(document).ready(function () {
	if (kelurahanPembeli) {
		getOngkir();
		kelurahanPembeli.addEventListener("change", getOngkir);
		kotaPembeli.addEventListener("change", function () {
			const checkout = document.querySelector('#checkout');
			checkout.setAttribute("disabled", "disabled");
		});
	}
})

function getOngkir() {
	const checkout = document.querySelector('#checkout');
	$.ajax({
		url: url + 'ajax/ambil_ongkir',
		type: 'post',
		dataType: 'json',
		data: {
			'idPenjual': idPenjual.value,
			'kelurahanPenjual': kelurahanPenjual.value,
			'kelurahanPembeli': kelurahanPembeli.value
		},
		beforeSend: function () {
			totalBayar.innerHTML = 'sedang menghitung';
		},
		success: function (success) {
			// row.style.display = 'none';
			console.log(success);
			if (success.status == 1) {
				$('#ongkir').data('ongkir', success.data.biaya_cod);
				// $('#ongkir').data('ongkir', 30)
				// alert($('#ongkir').data('ongkir'));
				$('#ongkir').html(convertToRupiah(success.data.biaya_cod));
				console.log(success.data.harga);
				hitungTotalBayar();
				checkout.removeAttribute("disabled");
				$('.info-ongkir').remove();
			} else {
				$('.info-ongkir').remove();
				checkout.setAttribute("disabled", "disabled");
				const labelBaru = document.createElement('div');
				labelBaru.classList.add('info-ongkir');
				labelBaru.classList.add('mt-2');
				labelBaru.classList.add('alert');
				labelBaru.classList.add('alert-info');
				const teksLabelBaru = document.createTextNode('Maaf, lokasi anda belum dijangkau oleh aplikasi.');
				labelBaru.appendChild(teksLabelBaru);

				// const box = document.querySelector('#checkout');

				$('#ongkir').data('ongkir', 0);
				$('#ongkir').html(convertToRupiah(0));
				checkout.after(labelBaru);
				hitungTotalBayar();
			}
		}
	});
}

// =====================================================================================================================================
// manajemen kategori produk
// =====================================================================================================================================
// const boxKategori = document.querySelector("#boxKategori");
// if (boxKategori) {
// 	boxKategori.addEventListener("change", function () {
// 		alert('oke');
// 	})
// }
let noKategori = 0;
$('#boxKategori').on('change', '#kategori', function () {
	// alert($(this).val())
	$.ajax({
		url: url + 'ajax/ambil_subkategori',
		type: 'post',
		dataType: 'json',
		data: {
			'id_parent': $(this).val(),
		},
		success: function (success) {
			console.log(success);
			if (success.status == 1) {
				if (noKategori > 0) {
					$(".kategori").last().remove();
					// document.querySelectorAll("#kategori").last().addClass("bg-primary");
				}
				let content = `<div class="col-md-4 kategori"><select class="form-control" name="kategori[]" id="kategori">
								 <option selected disabled>Pilih Sub Kategori</option>`;
				success.data.forEach(kel => {
					content += `
									<option value='${kel.id_kategori_produk}'>${kel.nama_kategori}</option>
									`;
				})
				content += `</select></div>`;
				$("#boxKategori").append(content);
				noKategori += 1;
				console.log(noKategori);
			} else {

			}
		}
	});
});


// cek kelurahan zonasi cod agar tidak menginput kelurahan yang sama
$("form#cod #kelurahan").on("change", function () {
	$(".info-kelurahan").html("");
	$.ajax({
		url: url + 'ajax/cek_kelurahan',
		type: 'post',
		dataType: 'json',
		data: {
			'kelurahan_id': $(this).val(),
		},
		success: function (success) {
			console.log(success);
			if (success.status == 1) {
				$(".info-kelurahan").html(`
					<div class="alert alert-info">
					Kelurahan ini sudah diinput dengan harga ${convertToRupiah(success.data.biaya_cod)}
					</div>
				`);
			} else {

			}
		}
	});
});

// checkbox penjual pasar
$('#penjualPasar').click(function () {
	if ($(this).prop("checked") == true) {
		$.ajax({
			url: url + 'ajax/ambil_pasar',
			type: 'post',
			dataType: 'json',
			success: function (success) {
				console.log(success);
				if (success.status == 1) {
					var el = `
						<select name="pasar" class="form-control mt-3" id="pilihPasar" placeholder="Pilih Pasar.">
							<option value="NULL" disabled selected>Pilih Pasar.</option>
					`;
					success.data.forEach(e => {
						el += `<option value="${e.id_pasar}">${e.nama_pasar}</option>`;
					})
					el += `</select>`;
				} else {
					var el = `
						<select name="pasar" class="form-control" id="pilihPasar" placeholder="Pilih Pasar.">
							<option value="NULL" disabled selected>Belum ada pasar yang tersedia.</option>
						</select>
					`;
				}
				$("#penjualPasar").parent().after(el);
			}
		});
		console.log("Checkbox is checked.");
	} else if ($(this).prop("checked") == false) {
		$('#pilihPasar').remove();
		console.log("Checkbox is unchecked.");
	}
});

// kirim diskusi
// checkbox penjual pasar
$('#kirimDiskusi').click(function () {
	if ($('#pesanDiskusi').val().length == 0) {
		alert("Pesan Tidak Boleh Kosong");
		$("#pesanDiskusi").focus();
		$("#pesanDiskusi").attr("placehoder", "Pesan harus diisi");
		$("#pesanDiskusi").addClass("is-invalid");
	} else {
		$(this).attr("disabled", "disabled");
		$(this).html('<i class="fas fa-spinner fa-spin"></i> Mengirim Pesan....');
		$.ajax({
			url: url + 'ajax/kirim_diskusi',
			type: 'post',
			data: {
				'id_produk': $("#idProduk").val(),
				'pesan': $("#pesanDiskusi").val()
			},
			dataType: 'json',
			success: function (success) {
				if (success.status == 1) {
					$("#pesanDiskusi").removeClass("is-invalid");
					$("#pesanDiskusi").addClass("is-valid");
					$("#kirimDiskusi").hide();
					$("#kirimDiskusi").after(`<div class="alert alert-success">Pesan anda berhasil dikirim.</div>`);
				} else {
					$("#kirimDiskusi").after(`<div class="alert alert-danger">Pesan anda gagal dikirim.</div>`);
					// $(this).html('Pesan anda gagal dikirim.');
				}
				setTimeout(function () {
					location.reload();
				}, 5000);
			}
		});
	}
});

// balas diskusi
$(".balasDiskusi").on("click", function (e) {
	e.preventDefault();
	$('#myModal').modal('show');
	$('#myModal').find('.modal-title').html("Balas Diskusi");
	const pesan = $(this).data("pesan");
	const idProduk = $(this).data("idproduk");
	const idDiskusi = $(this).data("iddiskusi");
	console.log(idProduk);
	const content = `
		<p>Pesan: <b>${pesan}</b></p>
		<div class="form-group">
			<input type="hidden" id="idProduk" value="${idProduk}">
			<input type="hidden" id="idDiskusi" value="${idDiskusi}">
			<label for="message-text" class="col-form-label">Balasan:</label>
			<textarea class="form-control" id="teksBalasanDiskusi"></textarea>
		</div>
	`;
	$('#myModal').find('.modal-body').html(content);

	$('#myModal').find('.btn-primary').html("Kirim Balasan");
	$('#myModal').find('.btn-primary').attr("id", "kirimBalasanDiskusi");
})

$('#myModal').on('click', '#kirimBalasanDiskusi', function () {
	const pesan = $('#teksBalasanDiskusi').val();
	if (pesan.length > 0) {
		const idProduk = $('#idProduk').val();
		const idDiskusi = $('#idDiskusi').val();
		$('#teksBalasanDiskusi').removeClass("is-invalid");
		$('.invalid-feedback').remove();
		$('#teksBalasanDiskusi').remove();
		const content = `<div class="alert alert-info"><i class="fas fa-spinner fa-spin fa-fw"></i> Sedang mengirim pesan.</div>`;
		$('#myModal').find('.modal-body').html(content);
		$(this).attr("disabled", "disabled");
		$.ajax({
			url: url + 'ajax/kirim_balasan_diskusi',
			type: 'post',
			data: {
				'id_produk': idProduk,
				'id_diskusi': idDiskusi,
				'pesan': pesan
			},
			dataType: 'json',
			success: function (success) {
				console.log(success);
				if (success.status == 1) {
					$('#myModal').find('.modal-body').html('<div class="alert alert-success"><i class="fas fa-check fa-fw"></i> Pesan terkirim.</div>');
				} else {
					$('#myModal').find('.modal-body').html('<div class="alert alert-danger"><i class="fas fa-times fa-fw"></i> Pesan gagal terkirim.</div>');
				}
				setTimeout(function () {
					location.reload();
				}, 3000);
			}
		});

	} else {
		$('#teksBalasanDiskusi').addClass("is-invalid");
		$('#teksBalasanDiskusi').after(`<div class="invalid-feedback">Pesan balasan tidak boleh kosong</div>`);
	}
});

// modal untuk menampilkan term-condition
$("#term-condition").on("click", function (e) {
	e.preventDefault();
	$('#myModal').modal('show');
	$('#myModal').find('.modal-title').html("Syarat dan Ketentuan");
	$.ajax({
		url: url + 'ajax/ambil_konten',
		type: 'post',
		data: {
			'field': 'term_condition'
		},
		dataType: 'json',
		success: function (success) {
			console.log(success);
			if (success.status == 1) {
				$('#myModal').find('.modal-body').html(success.data);
			}
		}
	});
})

// modal untuk menampilkan kebijakan privasi
$("#kebijakan-privasi").on("click", function (e) {
	e.preventDefault();
	$('#myModal').modal('show');
	$('#myModal').find('.modal-title').html("Kebijakan Privasi");
	$.ajax({
		url: url + 'ajax/ambil_konten',
		type: 'post',
		data: {
			'field': 'kebijakan_privasi'
		},
		dataType: 'json',
		success: function (success) {
			console.log(success);
			if (success.status == 1) {
				$('#myModal').find('.modal-body').html(success.data);
			}
		}
	});
})

$(".tutupModal").on("click", function () {
	$('#myModal').find('.modal-title').html("");
	$('#myModal').find('.modal-body').html("");
})

$(function () {
	$('[data-toggle="tooltip"]').tooltip()
});

// navbar
$(function () {
	'use strict'

	$("#btn-kategori").on("click", function () {
		// alert('oke');
		var trigger_id = $(this).attr('data-trigger');
		$(trigger_id).toggleClass("show");
		$('body').toggleClass("offcanvas-active");
	});

	// close if press ESC button 
	$(document).on('keydown', function (event) {
		if (event.keyCode === 27) {
			$("#navKategori").removeClass("show");
			$("body").removeClass("overlay-active");
		}
	});

	// close button 
	$(".btn-close").click(function (e) {
		$("#navKategori").removeClass("show");
		$("body").removeClass("offcanvas-active");
	});


})

// Prevent closing from click inside dropdown
$(document).on('click', '.dropdown-kategori', function (e) {
	e.stopPropagation();
});

// make it as accordion for smaller screens
if ($(window).width() < 992) {
	$('.dropdown-kategori a').click(function (e) {
		if ($(this).parent().parent().hasClass("submenu")) {

		} else {
			e.preventDefault();
		}

		if ($(this).next('.submenu').length) {
			$(this).next('.submenu').toggle();
		}


		$('.dropdown').on('hide.bs.dropdown', function () {
			$(this).find('.submenu').hide();
		})
	});
}

// tombol tawar
$("#tawar").on("click", function (e) {
	e.preventDefault();
	$('#myModal').modal('show');
	$('#myModal').find('.modal-title').html("Masukkan Penawaran");
	const idProduk = $(this).data("idproduk");
	console.log(idProduk);
	const content = `
		<div class="form-group">
			<input type="hidden" id="idProduk" value="${idProduk}">
			<input type="text" name="tawaran" id="hargaTawar" class="form-control" placeholder="Masukkan Penawaran Harga" onkeypress="hanyaAngka()" autocomplete="off">
		</div>
		<div class="alert alert-info">
			Penawaran hanya dapat dilakukan 1x untuk produk ini. Setelah tawaran pertama, penawaran berikutnya akan otomatis ditolak.
			<br>
			Setelah berhasil melakukan penawaran sebaiknya segera masukkan ke keranjang, jika anda keluar dari halaman detail produk,
			penawaran tidak akan berguna lagi.
		</div>
	`;
	$('#myModal').find('.modal-body').html(content);

	$('#myModal').find('.btn-primary').html("Kirim Penawaran");
	$('#myModal').find('.btn-primary').addClass("tawar");
	$("#hargaTawar").focus();
})

// $('#myModal').find('.tawar').on("click", function () {
// $('#myModal').find('btn-primary').on("click", ".tawar", function () {
	// $(document).on('click', '.tawar', function (e) {
	$("#myModal").on('click', '.tawar', function (e) {
	const idproduk = $('#myModal').find('#idProduk').val();
	const harga = $('#myModal').find('#hargaTawar').val();
	$(this).html("Mengirim Tawaran...");
	$(this).attr("disabled", "disabled");
	$('#myModal').find('.modal-body').html('');
	$.ajax({
		url: url + 'ajax/tawar_produk',
		type: 'post',
		data: {
			'idproduk': idproduk,
			'harga': harga
		},
		dataType: 'json',
		success: function (success) {
			console.log(success);
			let alert = "alert-success";

			if (success.status == "0") {
				alert = "alert-danger";
			}
			const content = `<div class="alert ${alert}">${success.msg}</div>`;
			$('#myModal').find('.modal-body').html(content);
			if (success.status == "1") {
				$("#harga").val(harga);
				$("#hargaLabel").html(convertToRupiah(harga));
				cekTotalHarga();
			}
			$('#myModal').find('.btn-primary').removeAttr("disabled");
			$('#myModal').find('.btn-primary').hide();
			// setInterval(() => {
			// 	$('#myModal').modal('hide');
			// }, 3000);
		}
	});
});

$('input.typeahead').typeahead({
	source: function (query, process) {
		return $.get(url + '/ajax/autocomplete', {
			query: query
		}, function (data) {
			data = $.parseJSON(data);
			return process(data);
		});
	}
});

// load produk lainnya.
const loadOther = $("#loadProdukOther");
if (loadOther) {
	const jumlahData = parseInt($("#jumlahProduk").val());
	if (jumlahData <= 23) {
		$("#loadProdukOther").hide();
	}
}

$("#loadProdukOther").on("click", function (e) {
	e.preventDefault();
	const scroll = $(window).scrollTop();

	const boxProduk = $("#container-produk");
	const idPenjual = $("#idPenjual").val();
	const penjualSeo = $("#penjualSeo").val();
	const jumlahData = parseInt($("#jumlahProduk").val());
	const page = parseInt($("#pageData").val());
	const dataView = page * 24 - 1;
	$.ajax({
		url: url + 'ajax/ambil_produk_lain',
		type: 'post',
		data: {
			'idpenjual': idPenjual,
			'page': dataView
		},
		dataType: 'json',
		beforeSend: function () {
			$(`<div class="my-2 text-center" id="loading"><i class="fas fa-spinner fa-spin"></i> Mengambil Produk.</div>`).insertAfter(boxProduk);
		},
		success: function (success) {
			$("#loading").remove();
			if (success.status == "1") {
				let content = ''
				success.data.forEach(el => {
					content += `
						<div class="col mb-3 p-1" id="product">
							<a href="${url}${penjualSeo}/${el.produk_seo}" class="text-decoration-none">
								<div class="card h-100 product rounded">
									<div class="thumbnail">
										<img src="${url}/uploads/produk/${el.gambar}" class="card-img-top rounded-top">
									</div>
									<div class="card-body">
										<div class="card-text">
											<b class="text-truncate text-dark d-block">${el.nama_produk}</b>
											<h6 class="text-primary font-weight-bolder">${convertToRupiah(el.harga_konsumen)}</h6>
										</div>
									</div>
								</div>
							</a>
						</div>
					`
				});
				boxProduk.append(content);
				$("body").scrollTo(scroll);
			}
		}
	});
	$("#pageData").val(page + 1);
	if ((parseInt($("#pageData").val()) * 24) > jumlahData) {
		$("#loadProdukOther").hide();
	}
});

$(".cariProdukByPenjual").on("click", function () {
	const keyword = $("#keywordProduk").val();
	if (keyword.length > 0) {
		const boxProduk = $("#container-produk");
		const idPenjual = $("#idPenjual").val();
		const penjualSeo = $("#penjualSeo").val();
		$.ajax({
			url: url + 'ajax/cari_produk_by_penjual',
			type: 'post',
			data: {
				'idpenjual': idPenjual,
				'keyword': keyword
			},
			dataType: 'json',
			beforeSend: function () {
				boxProduk.html(`<div class="my-2 text-center" id="loading"><i class="fas fa-spinner fa-spin"></i> Mengambil Produk.</div>`);
				$("#loadProdukOther").hide();
			},
			success: function (success) {
				$("#loading").remove();
				if (success.status == "1") {
					let content = ''
					success.data.forEach(el => {
						content += `
						<div class="col mb-3 p-1" id="product">
							<a href="${url}${penjualSeo}/${el.produk_seo}" class="text-decoration-none">
								<div class="card h-100 product rounded">
									<div class="thumbnail">
										<img src="${url}/uploads/produk/${el.gambar}" class="card-img-top rounded-top">
									</div>
									<div class="card-body">
										<div class="card-text">
											<b class="text-truncate text-dark d-block">${el.nama_produk}</b>
											<h6 class="text-primary font-weight-bolder">${convertToRupiah(el.harga_konsumen)}</h6>
										</div>
									</div>
								</div>
							</a>
						</div>
					`
					});
					boxProduk.html(content);
				} else {
					boxProduk.html(`<div class="alert alert-info">Produk Tidak Ditemukan.</div>`);
				}
			}
		});
	} else {
		alert("Masukkan Kata Kunci Produk");
		$("#keywordProduk").focus();
	}
})

$(".resetCariProdukByPenjual").on("click", function () {
	location.reload(true);
})

// popup
$("#closePopUp").on("click", function(){
	$(".container-popup").fadeOut(1000);
	$.ajax({
		url: url + 'ajax/tutup_iklan',
		dataType: 'json',
		success: function (success) {
			console.log(success);
		}
	});
})

$('.ignielToTop').on('mouseover', function(){
	// $('#textCallMe').css('display','inline-block');
	setTimeout(() => {
		$('#textCallMe').css('display','inline-block');
	}, 500);
})
$('.ignielToTop').on('mouseleave', function(){
	$('#textCallMe').css('display','none');
})

// tolak pesanan
$("#tolakPesanan").on("click", function(){
	$('#myModal').modal('show');
	$('#myModal').find('.modal-title').html("Kasih alasan kepada pembeli, kenapa anda menolak");
	const id = $(this).data("id");
	const content = `
		<div class="form-group">
			<input type="hidden" id="kodeTransaksi" value="${id}">
			<label for="message-text" class="col-form-label">Alasan anda:</label>
			<textarea class="form-control" id="keteranganPenjual"></textarea>
		</div>
	`;
	$('#myModal').find('.modal-body').html(content);

	$('#myModal').find('.btn-primary').html("Kirim");
	$('#myModal').find('.btn-primary').attr("id", "kirimTolakPesanan");
})

$('#myModal').on('click', '#kirimTolakPesanan', function () {
	const pesan = $('#keteranganPenjual').val();
	if (pesan.length > 0) {
		const id = $('#kodeTransaksi').val();
		$('#keteranganPenjual').removeClass("is-invalid");
		$('.invalid-feedback').remove();
		$('#keteranganPenjual').remove();
		const content = `<div class="alert alert-info"><i class="fas fa-spinner fa-spin fa-fw"></i> Sedang mengirim.</div>`;
		$('#myModal').find('.modal-body').html(content);
		$(this).attr("disabled", "disabled");
		$.ajax({
			url: url + 'ajax/tolak_pesanan',
			type: 'post',
			data: {
				'kode_transaksi': id,
				'pesan': pesan
			},
			dataType: 'json',
			success: function (success) {
				console.log(success);
				if (success.status == 1) {
					$('#myModal').find('.modal-body').html('<div class="alert alert-success"><i class="fas fa-check fa-fw"></i> Pesanan berhasil dibatalkan.</div>');
				} else {
					$('#myModal').find('.modal-body').html('<div class="alert alert-danger"><i class="fas fa-times fa-fw"></i> Pesanan gagal dibatalkan.</div>');
				}
				setTimeout(function () {
					location.reload();
				}, 3000);
			}
		});

	} else {
		$('#keteranganPenjual').addClass("is-invalid");
		$('#keteranganPenjual').after(`<div class="invalid-feedback">Alasan tidak boleh kosong</div>`);
	}
});

// promo
$("#kodePromo").on("keyup", function(){
	const kode = $(this).val();
	if(kode == ""){
		$("#keteranganPromo").hide();
	}else{
		$("#keteranganPromo").show();
	}

	$.ajax({
		url: url + 'ajax/cek_promo',
		type: 'post',
		data: {
			'kode': kode
		},
		dataType: 'json',
		success: function (success) {
			console.log(success);
			if (success.status == 1) {
				$("#keteranganPromo").html(`
					<span class="badge badge-${success.badge} text-wrap">${success.keterangan}</span>
				`)
			} else {
				$("#keteranganPromo").html(`<span class="badge badge-danger">Kode tidak ditemukan</span>`)
			}
		}
	});
})

// timepicker
$('#timepicker1').timepicker({
	uiLibrary: 'bootstrap4'
});
$('#timepicker2').timepicker({
	uiLibrary: 'bootstrap4'
});

// ==================================================================
// page promo
// ==================================================================
// salin kode promo
$(".salinKodePromo").on("click", function(){
	const trigger = $(this).data("trigger");
	const kode = $("#"+trigger).val();
	const el = document.createElement('textarea');
	el.value = kode;
	document.body.appendChild(el);
	el.select();
	document.execCommand('copy');
	document.body.removeChild(el);
})

// lihat syarat promo
$(".syaratPromo").on("click", function (e) {
	e.preventDefault();

	const title = $(this).data("title");
	const keterangan = $(this).data("keterangan");
	const gambar = $(this).data("gambar");
	let sdk = $(this).data("sdk");

	$('#myModal').modal('show');
	$('#myModal').find('.modal-title').html("S&K "+title);
	sdk = sdk.replace(/\r?\n/g, '<br />');
	const content = `
		<img src="${gambar}" alt="gambar-promo" class="img-fluid img-thumbnail">
		<p class="text-white text-center bg-primary my-2 py-1 small">Keterangan : ${keterangan}</p>
		<p class="bg-ligth shadow p-2 rounded-lg text-justify small">${sdk}</p>
	`;
	$('#myModal').find('.modal-body').html(content);

	$('#myModal').find('.btn-primary').hide();
})